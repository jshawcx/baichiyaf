<?php
/**
 * @name Bootstrap
 * @author root
 * @desc 所有在Bootstrap类中, 以_init开头的方法, 都会被Yaf调用,
 * @see http://www.php.net/manual/en/class.yaf-bootstrap-abstract.php
 * 这些方法, 都接受一个参数:Yaf_Dispatcher $dispatcher
 * 调用的次序, 和申明的次序相同
 */
class Bootstrap extends Yaf_Bootstrap_Abstract{

    public function _initConfig() {
        
        //错误设置
        define('ENVIRONMENT', strtolower(ini_get('yaf.environ')));
        ini_set('display_errors', 'on');
        if (defined('ENVIRONMENT')){
                switch (ENVIRONMENT)
                {
                        case 'dev':
                                error_reporting(E_ALL);
                        break;

                        case 'test':
                                error_reporting(E_ALL & ~E_NOTICE);
                        break;

                        case 'production':
                                error_reporting(0);
                        break;

                        default:
                                exit('The application environment is not set correctly.');
                }
        }
        
	//把配置保存起来
        $arrConfig = Yaf_Application::app()->getConfig();
        Yaf_Registry::set('config', $arrConfig);
        
    }
    
    public function _initDefine(){
        //文件路径
        // define('SERVER_DOMAIN','http://goyaf.com');//代码
        // define('STATIC_DOMAIN','http://static.goyaf.com');//js,css
        // define('IMG_DOMAIN','http://img.goyaf.com');//图片
         
        define('SERVER_DOMAIN','http://'.$_SERVER['HTTP_HOST']);
        define('STATIC_DOMAIN','http://'.$_SERVER['HTTP_HOST']);
        define('IMG_DOMAIN','http://'.$_SERVER['HTTP_HOST']);
        
        define('FUNC_PATH',    APPLICATION_PATH.'/application/function');//一些通用的方法函数

    } 
    
    public  function _initLibrary(){
        //载入通用的方法库
        Yaf_Loader::import(FUNC_PATH.'/Helper.php');
    }
    
    public function _initPlugin(Yaf_Dispatcher $dispatcher) {
            //注册一个插件
            $objSamplePlugin = new SamplePlugin();
            $dispatcher->registerPlugin($objSamplePlugin);
    }

    public function _initRoute(Yaf_Dispatcher $dispatcher) {
	//在这里注册自己的路由协议,默认使用简单路由
            $router = Yaf_Dispatcher::getInstance()->getRouter();

            $route_simple = new Yaf_Route_Simple("m","c","a");
            $router->addRoute('simple_api',$route_simple);//127.0.0.1?m=Api&c=Api&a=test
            
		$route_rewrite = new Yaf_Route_Rewrite(//127.0.0.1/longtugame2016
			'/longtugame2016',
			//'router/:param',
			//'list/:param/*',
			array(
                'module'=>'Api',
				'controller'=>'Api',
				'action'=>'test'
			)
		);
		$router->addRoute('list',$route_rewrite);

		$route_regex = new Yaf_Route_Regex(
		"#^/item/([0-9]+)#i",
			array(
                'module'=>'Index',
				'controller'=>'Share',
				'action'=>'index'
			),
			array(
			1 => 'id',
			)
		);
		$router->addRoute('router',$route_regex);
                
    }
	
    public function _initView(Yaf_Dispatcher $dispatcher){
	//在这里注册自己的view控制器，例如smarty,firekylin
    }

    public function _initLocalNamespace(){
	//在这里注册自己的本地类，例如smarty,firekylin
        $loader = Yaf_Loader::getInstance();
        $loader->registerLocalNamespace(array("Db","Base","Cache"));
    }

}
