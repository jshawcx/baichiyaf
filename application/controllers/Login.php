<?php
/**
 * @name IndexController
 * @author root
 * @desc 默认控制器
 * @see http://www.php.net/manual/en/class.yaf-controller-abstract.php
 */
class LoginController extends Base_WebCommon {
	public function init(){
	}
	/** 
     * 默认动作
     * Yaf支持直接把Yaf_Request_Abstract::getParam()得到的同名参数作为Action的形参
     * 对于如下的例子, 当访问http://yourhost/Sample/index/index/index/name/root 的时候, 你就会发现不同
     */
	public function indexAction() {
            //登陆页
            if(Yaf_Session::getInstance()->has('yaf_login')){
                header('Location:/admin/index/index'); exit;   
            }
	}
/*
 * ajax登陆
 */
	public function loginAction(){
		//$this->getRequest()->getMethod();
		$username = $this->getRequest()->getPost('username');
		$password = $this->getRequest()->getPost('password');

		$login = new LoginModel();

		$res = $login->login($username,$password);
		$data= array();
			if($res){
                            //统计登录次数,最后一次登录的ip和时间和账号
                            $yac = new Yac();
                            //暂时保存登录的信息
                            $login_info = array(
                                'ip'=>$_SERVER['REMOTE_ADDR'],
                                'time'=>date('Y-m-d H:i:s',time()),
                                'account'=>$username
                            );
                            $yac->set('login_info',$login_info);
                            
				$data['status'] = 0;
				$data['msg'] = '登陆成功';
				$data['res'] = array();
			}else{
				$data['status'] = 1;
				$data['msg'] = '账号或密码错误';
				$data['res'] = array();
			}
			echo json_encode($data);exit;
	}

        
/*
 * ajax登出
 */        
    public function logoutAction(){
       
        $data= array();    
        if(Yaf_Session::getInstance()->has('yaf_login')){
            $status = Yaf_Session::getInstance()->del('yaf_login');
            if($status){
                
                $yac = new Yac();
                
                $login_info = $yac->get('login_info');
                $login_count = $yac->get('login_count');
                
                if($login_count){
                    $count = $login_count['count'];
                }else{
                    $count = 0;
                }
                $arr_set = array(
                    'count' => $count + 1,
                    'last_ip' => $login_info['ip'],
                    'last_time' => $login_info['time'],
                    'account' => $login_info['account'],
                );
                $yac->set('login_count', $arr_set);

                $data['status']=0;
                $data['msg']='登出成功!';
            }else{
                $data['status']=1;
                $data['msg']='登出失败!';
            }    
        }else{
            $data['status']=0;
            $data['msg']='登出成功!';
        }
        echo json_encode($data);exit;
    } 
    
}
