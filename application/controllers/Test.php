<?php
/**
*测试用
*/
class TestController extends Base_WebCommon{
	public function init(){
		parent::init();
	}
		/*
		*生成excel
		*/
        public function outputExcelAction(){
            
            error_reporting(E_ALL);
            date_default_timezone_set('Asia/Shanghai');
            
            Yaf_loader::import("PHPExcel.php") ;
            
            $data= array();
            $nextnumber=1111;
            $pass_all = $this->unique_rand(100000000, 888888888, 5000);
           
            for($i=0;$i<5000;$i++){
                $nextnumber+=1;
                $number ='BC0A'.date('ym',time()).$nextnumber;
                $data[$i]['number']=$number;
                $data[$i]['pass']=  $pass_all[$i]+date('ymd',time());
            }
            
            $objPHPExcel=new PHPExcel();
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1','卡号')
                        ->setCellValue('B1','密码');
            $i=2;			
            foreach($data as $k=>$v){
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i,$v['number'])
                        ->setCellValue('B'.$i,' '.$v['pass'].' ');
                    $i++;
            }
            $objPHPExcel->getActiveSheet()->setTitle('卡券生成');
            $objPHPExcel->setActiveSheetIndex(0);
            $filename='卡券A';

            //生成xls文件
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.$filename.'.xls"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            $objWriter->save('php://output');
            exit;
            
        }

        /**
        *随机数
        */
        public function unique_rand ($min, $max, $num){
            $count = 0;
            $return = array();
            while ($count < $num) {
                $return[] = mt_rand($min, $max);
                $return = array_flip(array_flip($return));
                $count = count($return);
            }
            shuffle($return);
            return $return;
        }


}