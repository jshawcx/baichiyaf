<?php

/**验证码辅助函数
 * @author jshawcx
 * @see https://github.com/newbie007/goyaf
 */

//获取验证码
function chk_code($str){
	return (strtoupper($str)==$_SESSION['code'])?TRUE:FALSE;
}
