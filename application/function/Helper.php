<?php
/**
 * File: Helper.php
 * Functionality: 公共函数
 * Author: https://github.com/newbie007
 * Date: 2015-9-9
 */
abstract class Helper{
    
public static function import($file) {
		$function = ucfirst($file);
		$f_file   = FUNC_PATH.'/'.$function.'.php';

		if(file_exists($f_file)){
			Yaf_Loader::import($f_file);
			unset($function, $f_file);
		}else{
			$traceInfo = debug_backtrace();

			$error = 'Function '.$f_file.' NOT FOUND !';
			throw new Exception($error);
		}
	}

}



