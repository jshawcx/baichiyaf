<?php

/**图片处理辅助函数
 * @author jshawcx
 * @see https://github.com/newbie007/goyaf
 */

/*
 * 保存图片
 * img:64位图片的编码,dir:保存的文件夹名称
 */
function image_save($img,$dir='image64') {
            
            //Yaf_Loader::import("upyun.class.php");
            
            $dir_ini = dirname($_SERVER['DOCUMENT_ROOT']).'/conf/application.ini';
            $config = new Yaf_Config_Ini($dir_ini,'account');
       
            $bucket = $config->upyun->bucket;
            $upyun_user = $config->upyun->user;
            $upyun_psw = $config->upyun->pwd;
            
            $curDateTime = date("YmdHis");
            $ymd = date("Ymd");
	    $randNum = rand(1000, 9999);
            $fileName = $curDateTime . $randNum;
	    $pathdir = 'upload/'.$dir.'/';
	    $attached_type = '';
		if(strstr($img,'data:image/jpeg;base64,')) {
			$img_base = str_replace('data:image/jpeg;base64,', '', $img);
			$attached_type = 'jpg';
		} elseif(strstr($img,'data:image/png;base64,')) {
			$img_base = str_replace('data:image/png;base64,', '', $img);
			$attached_type = 'png';
		} elseif(strstr($img,'data:image/gif;base64,')) {
			$img_base = str_replace('data:image/gif;base64,', '', $img);
			$attached_type = 'gif';
		} else {
			return $img;
		};
		if($attached_type!='') {
			$img_decode = base64_decode($img_base);
            //如果账号不为空传到upyun
            if($bucket&&$upyun_user&&$upyun_psw){
                    $upyun = new UpYun($bucket, $upyun_user, $upyun_psw);
                    try {
                            $fh = $img_decode;
                            $newfileurl='/'. $dir .'/'.$fileName.'.'.$attached_type;
                            $upinfo = $upyun->writeFile($newfileurl, $fh, True);   // 上传图片，自动创建目录
                            $file_url = "http://".$bucket.".b0.upaiyun.com".$newfileurl;
                    }catch(Exception $e) {
                        //echo $e->getCode();
                        $file_url = array();
                        $file_url['msg'] =  $e->getMessage();
                    }
            }else{
            	
                    $fullName = $pathdir.$fileName.'.'.$attached_type; // 完整路径
                    if (!is_dir($pathdir)){
                            mkdir($pathdir, 0777,true); // 使用最大权限0777创建文件
                    };
                    if (!file_exists($fullName)) { // 如果不存在则创建
                            // 检测是否有权限操作
                            if (!is_writeable($fullName)) {
                                    @chmod($fullName, 0777); // 如果无权限，则修改为0777最大权限
                            };
                            // 最终将d写入文件即可
                            file_put_contents($fullName, $img_decode);
                    };
                    $file_url = 'http://'.$_SERVER['HTTP_HOST'].'/'.$fullName;
            }
			return $file_url;
		};
}


