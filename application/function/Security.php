<?php

/**安全辅助函数
 * @author jshawcx
 * @see https://github.com/newbie007/goyaf
 */

    //加密
    function encryption($value){
        Yaf_loader::import("AppSecret/secret.php");
        $secret = new secret();
        $encrypt_str = $secret->encrypt($value);
        return $encrypt_str;
    }
    
    //解密
    function decryption($value){
        Yaf_loader::import("AppSecret/secret.php");
        $secret = new secret();
        $decrypt_str = $secret->decrypt($value);
        return $decrypt_str;
    }