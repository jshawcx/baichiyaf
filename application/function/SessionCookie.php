<?php
  
//设置session
  function set_session($key, $val){
    return Yaf_Session::getInstance()->__set($key, $val);
  }
  
  //得到session
  function get_session($key){
    return Yaf_Session::getInstance()->__get($key);
  }
  
  //是否设置session
  function has_session($key){
    return Yaf_Session::getInstance()->has($key);
  }
  
//销毁session
  function unset_session($key){
    return Yaf_Session::getInstance()->__unset($key);
  }
  
 //Set COOKIE
  function set_cookie($key, $value, $expire = 86400, $path = '/'){
    $expire = time()+$expire;
    setcookie($key, $value, $expire, $path);
  }

 //获取cookie
  function get_cookie($key){
      $cookie = isset($_COOKIE[$key])?$_COOKIE[$key]:'';
      return $cookie;
  }

  // Clear cookie
  function clear_cookie($key){
    $this->set_cookie($key, '');
  }