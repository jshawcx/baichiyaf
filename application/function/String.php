<?php

/**字符串辅助函数
 * @author jshawcx
 * @see https://github.com/newbie007/goyaf
 */


/**
 * 随机字符串
 *
 * @return bool
 **/
function random($length, $numeric = 0) {
	$seed = base_convert(md5(microtime().$_SERVER['DOCUMENT_ROOT']), 16, $numeric ? 10 : 35);
	$seed = $numeric ? (str_replace('0', '', $seed).'012340567890') : ($seed.'zZ'.strtoupper($seed));
	if($numeric) {
		$hash = '';
	} else {
		$hash = chr(rand(1, 26) + rand(0, 1) * 32 + 64);
		$length--;
	}
	$max = strlen($seed) - 1;
	for($i = 0; $i < $length; $i++) {
		$hash .= $seed{mt_rand(0, $max)};
	}
	return $hash;
}

/**
 * 截断字符串
 * sourcestr:字符串,cutlength:长度
 */
function str_cut($sourcestr,$cutlength) {
	$returnstr='';
	$i=0;
	$n=0;
	$str_length=strlen($sourcestr);//字符串的字节数
	while (($n<$cutlength) and ($i<=$str_length)) {
		$temp_str=substr($sourcestr,$i,1);
		$ascnum=Ord($temp_str);//得到字符串中第$i位字符的ascii码
		if ($ascnum>=224)    //如果ASCII位高与224，
		{
		$returnstr=$returnstr.substr($sourcestr,$i,3); //根据UTF-8编码规范，将3个连续的字符计为单个字符
		$i=$i+3;            //实际Byte计为3
		$n++;            //字串长度计1
		}
		elseif ($ascnum>=192) //如果ASCII位高与192，
		{
		$returnstr=$returnstr.substr($sourcestr,$i,2); //根据UTF-8编码规范，将2个连续的字符计为单个字符
		$i=$i+2;            //实际Byte计为2
		$n++;            //字串长度计1
		}
		elseif ($ascnum>=65 && $ascnum<=90) //如果是大写字母，
		{
		$returnstr=$returnstr.substr($sourcestr,$i,1);
		$i=$i+1;            //实际的Byte数仍计1个
		$n++;            //但考虑整体美观，大写字母计成一个高位字符
		}
		else                //其他情况下，包括小写字母和半角标点符号，
		{
		$returnstr=$returnstr.substr($sourcestr,$i,1);
		$i=$i+1;            //实际的Byte数计1个
		$n=$n+0.5;        //小写字母和半角标点等与半个高位字符宽…
		}
	}
	if ($str_length>$cutlength){
		$returnstr = $returnstr . '…';//超过长度时在尾处加上省略号
	}
	return $returnstr;
}


/**
 * 截取中英混排字符串
 * @param (string) $string
 * @param (int) $length
 * @param (string) $dot
 * @param (string) $charset
 */
function str_cut_two( $string, $length, $dot = '..', $charset='utf-8' ) {
	$slen = strlen($string);
    if( $slen <= $length ) {
        return $string;
    }
	if( function_exists( 'mb_substr' ) ) {
		return mb_substr( $string, 0, $length, $charset ) . $dot;
	}
    $strcut = '';
    if(strtolower($charset) == 'utf-8') {
        $n = $tn = $noc = 0;
        while($n < $slen) {
            $t = ord($string[$n]);
            if($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
                $tn = 1; $n++; $noc++;
            } elseif(194 <= $t && $t <= 223) {
                $tn = 2; $n += 2; $noc += 1;
            } elseif(224 <= $t && $t < 239) {
                $tn = 3; $n += 3; $noc += 1;
            } elseif(240 <= $t && $t <= 247) {
                $tn = 4; $n += 4; $noc += 1;
            } elseif(248 <= $t && $t <= 251) {
                $tn = 5; $n += 5; $noc += 1;
            } elseif($t == 252 || $t == 253) {
                $tn = 6; $n += 6; $noc += 1;
            } else {
                $n++;
            }
            if($noc >= $length) {
                break;
            }
        }
        if($noc > $length) {
            $n -= $tn;
        }
        $strcut = substr($string, 0, $n);
    } else {
        for($i = 0; $i < $length; $i++) {
            $strcut .= ord($string[$i]) > 127 ? $string[$i].$string[++$i] : $string[$i];
        }
    }
    
    return $strcut.$dot;
}


//实体转换为字符
function str_strip_tags($str, $allowable_tags = ""){
    $str = html_entity_decode($str, ENT_QUOTES, 'UTF-8');
    return strip_tags($str, $allowable_tags);
}
    
//替换正文BASE64图片链接
function str_image_base64($content) {
	preg_match_all("/\<img.*?src\=\"(.*?)\"[^>]*>/i", $content, $match);
	foreach($match[1] as $img) {
		$content = str_replace($img,mc_save_img_base64($img),$content);
	};
	return $content;
}
    

    
 //简单格式化数据  
function str_magic_in($content) {
	if(get_magic_quotes_gpc()){ 
	    $val = $content;
	} else {
	   	$val = addslashes($content);
	};
	return $val;
}

//简单还原格式化数据
function str_magic_out($content) {
	$content1 = str_replace('\r\n', ' ', $content);
	$val = stripslashes($content1);
	return $val;
}


/*
 *过滤字符串
 */
function str_filter($arr){
    if (!isset($arr)) {
        return null;
    }
    if (is_array($arr)) {
        foreach ($arr as $k => $v) {
            $arr[$k] = str_magic_in(strip_sql_chars(strip_html(trim($v), true)));
        }
    } else {
        $arr =  str_magic_in(strip_sql_chars(strip_html(trim($arr), true)));
    }

    return $arr;
}


//无编辑器的过滤
function str_filter_two($str){
	
	$pattern="/<pre[^>]*>(.*?)<\/pre>/si";
	preg_match_all($pattern, $str, $matches);
	$str=htmlspecialchars_decode($str);
	$str=stripslashes($str);
	if($matches[1]){
		foreach($matches[1] as $v){
			$replace[]= addslashes(htmlspecialchars(trim($v)));
		}
		$str = str_replace($matches[1], $replace, $str);
	} else{
		$str=strip_tags($str,"<img> <pre> <a> <font> <span> <em>");
	}
	$str = nl2br($str);
	
	return $str;
}


//过滤html
function strip_html($content, $xss = true) {
    $search = array("@<script(.*?)</script>@is",
        "@<iframe(.*?)</iframe>@is",
        "@<style(.*?)</style>@is",
        "@<(.*?)>@is"
    );

    $content = preg_replace($search, '', $content);

    if($xss){
        $ra1 = array('javascript', 'vbscript', 'expression', 'applet', 'meta', 'xml', 'blink', 'link', 
        'style', 'script', 'embed', 'object', 'iframe', 'frame', 'frameset', 'ilayer', 
        'layer', 'bgsound', 'title', 'base');
                                
        $ra2 = array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy',      'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');
        $ra = array_merge($ra1, $ra2);
        
        $content = str_ireplace($ra, '', $content);
    }

    return strip_tags($content);
}

/**
 * 清除HTML标记
 *
 * @param	string	$str
 * @return  string
 */
function strip_html_two($str){
	$str = strip_tags($str);
	$str = htmlspecialchars($str);
	$str=preg_replace("/\s+/"," ", $str); //过滤多余回车
	 $str = preg_replace("/ /","",$str);
	 $str = preg_replace("/&nbsp;/","",$str);
	 $str = preg_replace("/　/","",$str);
	 $str = preg_replace("/\r\n/","",$str);
	 $str = str_replace(chr(13),"",$str);
	 $str = str_replace(chr(10),"",$str);
	 $str = str_replace(chr(9),"",$str);
	return $str;
}

/**
 *  去除mysql 保留字
 */
function strip_sql_chars($str) {
    $replace = array('SELECT', 'INSERT', 'DELETE', 'UPDATE', 'CREATE', 'DROP', 'VERSION', 'DATABASES',
        'TRUNCATE', 'HEX', 'UNHEX', 'CAST', 'DECLARE', 'EXEC', 'SHOW', 'CONCAT', 'TABLES', 'CHAR', 'FILE',
        'SCHEMA', 'DESCRIBE', 'UNION', 'JOIN', 'ALTER', 'RENAME', 'LOAD', 'FROM', 'SOURCE', 'INTO', 'LIKE', 'PING', 'PASSWD');
    
    return str_ireplace($replace, '', $str);
}
