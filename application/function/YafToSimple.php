<?php
//用户自定义函数(不通用的函数)

    /**
     * 获取用户头像
     */
    function getavatar($uid = 0) {
        $suid = sprintf("%09d", $uid);
        $dir1 = substr($suid, 0, 3);
        $dir2 = substr($suid, 3, 2);
        $dir3 = substr($suid, 5, 2);
        $rootDir = '/uploadfile/avatar/';
        $userDir = $dir1.'/'.$dir2.'/'.$dir3.'/';
        $fileDir = $rootDir.$userDir.$uid.'_avatar.jpg';
        return $fileDir;
    }   
