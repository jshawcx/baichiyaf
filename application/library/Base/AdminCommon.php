<?php
/*
 * 后台控制器基础类
 */
class Base_AdminCommon extends Yaf_Controller_Abstract{
	function init(){
            if(!Yaf_Session::getInstance()->has('yaf_login')){
               header('Location:/login'); exit;   
            }
	    if($this->getRequest()->isxmlHttpRequest()){
			Yaf_Dispatcher::getInstance()->disableView();
            }
            Helper::import('string');//加载字符串辅助函数
            Helper::import('common');//加载通用函数
            Helper::import('yafToSimple');//加载自定义个别函数
            Helper::import('SessionCookie');
	}
        
      
//从客户端返回变量，这个方法将从请求url中寻找参数name      
  public function get($key, $filter = TRUE){

        if($filter){
          return str_filter($this->getRequest()->get($key));
        }else{
          return $this->getRequest()->get($key);
        }
  }
  
//获取post变量,空返回全部
  public function get_post($key='', $filter = TRUE){
    
    if($key==''){
        return $this->getRequest()->getPost();
    }else{
        if($filter){
          return str_filter($this->getRequest()->getPost($key));
        }else{
          return $this->getRequest()->getPost($key);
        }
    }
    
  }
  
//获取GET变量,空返回全部
  public function get_query($key='', $filter = TRUE){
     
    if($key==''){
        return $this->getRequest()->getQuery();
    }else{
        if($filter){
          return str_filter($this->getRequest()->getQuery($key));
        }else{
          return $this->getRequest()->getQuery($key);
        }
    }
    
  }
        
        
}