<?php

/**
* 前台控制器基础类
*/
class Base_WebCommon extends Yaf_Controller_Abstract{

	function init(){
		if($this->getRequest()->isxmlHttpRequest()){
			Yaf_Dispatcher::getInstrance()->disableView();
		}
	}
	
}