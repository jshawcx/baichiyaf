<?php
/*
 * 数据库基础类
 */
class Db_BaseDb extends Db_MysqliDb{

        protected $db;
        private $db_config;
        public function __construct($status='read'){
            
            if($status=='read'){
                $this->db_config = Yaf_Registry::get('config')->database->slave1;
            }else{
                $this->db_config = Yaf_Registry::get('config')->database->default;
            }   
                
		$host = $this->db_config->host;
		$username = $this->db_config->username;
		$password = $this->db_config->password;
		$db = $this->db_config->database;
		$port = $this->db_config->port;
                $prefix = $this->db_config->prefix;
                
		$this->db = new Db_MysqliDb($host,$username,$password,$db,$port);
                $this->db->setPrefix ($prefix);
	}
	
}