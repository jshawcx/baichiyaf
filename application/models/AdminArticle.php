<?php
//资讯管理model
class AdminArticleModel extends Db_BaseDb{
    
    //private $pagesize = 10;
    private $table = 'article';


    public function __construct($status ='read') {
        parent::__construct($status);
    }
    
    //上架或下架资讯
    public function article_status($id,$type){
        if($type=='start'){
            $data = Array (
                'status' => 1,
            );
        }else if($type=='stop'){
            $data = Array (
                'status' => 0,
            );
        }
        
        $this->db->where ('id',$id);
        if ($this->db->update ($this->table, $data)){
            $res['status']=200;
            $res['msg'] = $this->db->count.'条数据更新';
        }else{
            $res['status']=500;
            $res['msg'] = $this->db->getLastError();
        }
        return json_encode($res);
    }

    
}