<?php
//会员管理model
class AdminMemberModel extends Db_BaseDb{
    
    //private $pagesize = 10;
    private $table = 'member';


    public function __construct($status ='read') {
        parent::__construct($status);
    }
    
    //上架或下架
    public function member_status($id,$type){
        if($type=='start'){
            $data = Array (
                'islock' => 0,
            );
        }else if($type=='stop'){
            $data = Array (
                'islock' => 1,
            );
        }
        
        $this->db->where ('userid',$id);
        if ($this->db->update ($this->table, $data)){
            $res['status']=200;
            $res['msg'] = $this->db->count.'条数据更新';
        }else{
            $res['status']=500;
            $res['msg'] = $this->db->getLastError();
        }
        return json_encode($res);
    }

    
}