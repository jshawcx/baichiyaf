<?php
//公告管理model
class AdminNoticeModel extends Db_BaseDb{
    
    //private $pagesize = 10;
    private $table = 'announce';


    public function __construct($status ='read') {
        parent::__construct($status);
    }
    
    //上架或下架资讯
    public function article_status($id,$type){
        if($type=='start'){
            $data = Array (
                'passed' => 1,
            );
        }else if($type=='stop'){
            $data = Array (
                'passed' => 2,
            );
        }
        
        $this->db->where ('announceid',$id);
        if ($this->db->update ($this->table, $data)){
            $res['status']=200;
            $res['msg'] = $this->db->count.'条数据更新';
        }else{
            $res['status']=500;
            $res['msg'] = $this->db->getLastError();
        }
        return json_encode($res);
    }

    
}