<?php

class ApiModel extends Db_BaseDb{
    public function __construct($status = 'read') {
        parent::__construct($status);
    }
    
    /**
     * 获取列表,
     * @param string 表名
     * @param array or string 查询的列或*
     * @param string 过滤条件
     * @param array 分页
     * @param array 排序
     * @param array or boole $join 是否关联查询 array，false
     * @return array
     */   
    public  function list_common($table,$cols,$search_where,$limit,$orderby,$join=false){
        
            if($join!== false){
                $this->db->join($join[0],$join[1],$join[2]);
            }
            
            if(!empty($search_where)){
                foreach ($search_where as $v) {
                    if($v!=''){
                        $this->db->where($v);
                    }
                }
            }
            foreach($orderby as $k=>$v){
                $this->db->orderBy($k,$v);
            }
            
            $result = $this->db->get($table,$limit,$cols);
            
            return $result;
    }
    
    
    /**
     * 更具id获得信息
     * @param string $table 表名
     * @param string $cols 查询的列或*
     * @param array $search_where  条件
     * @param array or boole $join 是否关联查询 array，false
     * @return array 得到的数据
     */
    
    public function info_common($table,$cols,$search_where,$join = false){
        if($join!== false){
          $this->db->join($join[0],$join[1],$join[2]);
        }
        foreach($search_where as $v){
            $this->db->where ($v);
        }
        $user = $this->db->getOne ($table,$cols);
        return $user;
    }
    
    /**
     * 
     * @param string $table 表名
     * @param int $pro_id 商品id
     * $return array  查询列表
     */
    function  order_status($table,$pro_id){
        //订单状态：$this->status = array('3'=>'待审核', '7'=>'审核通过','4'=>'审核失败','0'=> '已关闭', '6'=>'申诉中');//'2'=>'待填订单',默认是二
        //查询 2,7
       $this->db->where('goods_id',$pro_id);
       $this->db->where('status', Array(3,2,7,4,0,6), 'IN');
       $results = $this->db->get($table,null,array('status','buyer_id'));
        return $results;
        
    }
    
   /**
     * 
     * @param string $table 表名
     * @param string $field 字段名
     * @param int $value 数值
     * @param array $status 订单状态
     * $return array  统计总数
     */
    
    function order_count($table,$field,$value,$status){
       $this->db->where($field,$value);
       $this->db->where('status', $status, 'IN');
       $results = $this->db->get($table,null,array('id'));
       $count = $this->db->count;
        return $count;
    }
    
    
    /**
     * 更具id删除数据
     *@param string $table 表名
     * @param int $id 数据id
     * @param string $field 表的字段
     *@return array
     */
    public function del_common($table,$field,$ids){
        if(is_numeric($ids)){
             $id = intval($ids);
            $this->db->where($field, $id);
        }else{
            $id_all = explode(',',$ids);
            $this->db->where($field, $id_all, 'IN');
        }
        
            if($this->db->delete($table)){
                $res['status']=200;
                $res['msg']='删除成功!';
            }else{
                $res['status']=500;
                $res['msg']='删除失败';
            }
        
        return $res;
    }
    
    /**
     *通用的新增
     * @param string 表名
     * @param array 数组格式的数据
     * @return array 返回数据
     */
    public function add_common($table,$data){
        
            $insert_id =  $this->db->insert ($table,$data); 
            if($insert_id > 0){
                $status =200;
                $msg = '保存成功,id号:'.$insert_id;
                $id = $insert_id;
            }else{
                $status =500;
                $msg = '保存失败,错误:'.$this->db->getLastError();
                $id = 0;
            }
        
        $res['status'] = $status;
        $res['msg'] = $msg;
        $res['id'] = $id;
        return $res;
    }
    
    
    /**
     *通用的编辑修改方法
     * @param string 表名
     * @param string $field 表的字段
     * @param int 编辑的字段
     * @param array 数组格式的数据
     * @return array 返回数据
     */
    public function update_common($table,$field,$id,$data){

        $this->db->where($field, $id);
        if ($this->db->update($table, $data)) {
            $status = 200;
            $msg = $this->db->count . '项被修改!';
        } else {
            $status = 500;
            $msg = '修改失败,错误:' . $this->db->getLastError();
        }

        $res['status'] = $status;
        $res['msg'] = $msg;
        $res['id'] = $id;
        return $res;
    }
    
    
}

