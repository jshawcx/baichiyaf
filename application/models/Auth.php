<?php

class AuthModel extends Db_BaseDb{
    public function __construct($status = 'read') {
        parent::__construct($status);
        $this->weixin = new WeixinModel();
    }

	//每一个页面权限
	public  function   verification(){
	    $info=$this->weixin->getYz();
	    return  $info;
	}

	//js token
	public  function   getSignPackage(){
	    $info=$this->weixin->getSignPackage();
	    return  $info;
	}

}

