<?php
/**
 * @name SampleModel
 * @desc sample数据获取类, 可以访问数据库，文件，其它系统等,公用的方法,写入,读取
 * @author root
 */
class SampleModel extends Db_BaseDb{
    public function __construct($status ='read') {
        parent::__construct($status);
    }   
    
    
    /**
     * 获取列表,一般jquerytables使用
     * @param int 原样返回数据
     * @param string 表名
     * @param array or string 查询的列或*
     * @param string 过滤条件
     * @param array 分页
     * @param array 排序
     * @param array or boole $join 是否关联查询 array，false
     * @return json
     */   
    public  function list_common($draw,$table,$cols,$search_where,$limit,$orderby,$join=false){
       
            if($join!== false){
                $this->db->join($join[0],$join[1],$join[2]);
            }
            if(isset($_GET['modelid']) && $_GET['modelid']==1){//只用在member表判断是普通会员
                $this->db->where('modelid = 1');
            }
            if(isset($_GET['modelid']) && $_GET['modelid']==2){//只用在member表判断是商家
                $this->db->where('modelid = 2');
            }
           $result_all = $this->db->get($table);
           $data_count = $this->db->count;
           
           
            if($join!== false){
                $this->db->join($join[0],$join[1],$join[2]);
            }
            if(!empty($search_where)){
                foreach ($search_where as $v) {
                    $this->db->where($v);
                }
            }
            foreach($orderby as $k=>$v){
                $this->db->orderBy($k,$v);
            }
            $result = $this->db->get($table);
            $data_count_where = $this->db->count;
           
           
            if($join!== false){
                $this->db->join($join[0],$join[1],$join[2]);
            }
            if(!empty($search_where)){
                foreach ($search_where as $v) {
                    $this->db->where($v);
                }
            }
            foreach($orderby as $k=>$v){
                $this->db->orderBy($k,$v);
            }
            $result = $this->db->get($table,$limit,$cols);

            
               $ret = array(
                       "draw" => $draw,  
                       "recordsTotal" => $data_count,  //即没有过滤的记录数（数据库里总共记录数）
                       "recordsFiltered" => $data_count_where,  //过滤后的记录数（如果有接收到前台的过滤条件，则返回的是过滤后的记录数） 
                       "data" => $result, //表中中需要显示的数据 
                   );
               return json_encode($ret);
    }
    
    /**
     * 通用的查询数据
     * @param string $table 表名
     * @param string $cols 查询的列或*
     * @param array 分页
     * @param array 排序
     * @param string 过滤条件
     * @return array
     */
    public function list_common_normal($table,$cols,$limit,$orderby,$search_where=''){
            if(!empty($search_where)){
                foreach ($search_where as $v) {
                    $this->db->where($v);
                }
            }
            foreach($orderby as $k=>$v){
                $this->db->orderBy($k,$v);
            }
            $result = $this->db->get($table,$limit,$cols);
            return $result;
    }


    /**
     * 更具id获得信息
     * @param string $table 表名
     * @param string $cols 查询的列或*
     * @param string $field 表的字段
     * @param int $id 数据id
     * @param array or boole $join 是否关联查询 array，false
     * @return array 得到的数据
     */
    public function info_common($table,$cols,$field,$id,$join = false){
        if($join!== false){
          $this->db->join($join[0],$join[1],$join[2]);
        }
        $this->db->where ($field,$id);
        $user = $this->db->getOne ($table,$cols);
        return $user;
    }

    /**
     * 更具id获得信息
     * @param string $table 表名
     * @param string $cols 查询的列或*
     * @param array $search_where  条件
     * @param array or boole $join 是否关联查询 array，false
     * @return array 得到的数据
     */
    
    public function info_common_where($table,$cols,$search_where,$join = false){
        
        if($join!== false){
          $this->db->join($join[0],$join[1],$join[2]);
        }
        foreach($search_where as $v){
            $this->db->where ($v);
        }
        $user = $this->db->getOne ($table,$cols);
        return $user;
    }
    
    
    
    /**
     *通用的新增或编辑方法
     * @param string 表名
     * @param string $field 表的字段
     * @param int 编辑的id号,新增默认0
     * @param array 数组格式的数据
     * @param string 是否忽略id强制新增数据,new或''
     * @return array 返回数据
     */
    public function add_edit_common($table,$field,$id,$data,$id_status=''){
        
        if($id>0){
            if($id_status!=''){
                $insert_id =  $this->db->insert ($table,$data); 
                if($insert_id > 0){
                    $status =200;
                    $msg = '保存成功,id号:'.$insert_id;
                    $id = $insert_id;
                }else{
                    $status =500;
                    $msg = '保存失败,错误:'.$this->db->getLastError();
                }
            }else{
                $this->db->where ($field, $id);
                if ($this->db->update ($table, $data)){
                    $status =200;
                    $msg = $this->db->count.'项被修改!';
                }else{
                     $status =500;
                    $msg='修改失败,错误:'.$this->db->getLastError();
                } 
            }

            
        }else{
            $insert_id =  $this->db->insert ($table,$data); 
            if($insert_id > 0){
                $status =200;
                $msg = '保存成功,id号:'.$insert_id;
                $id = $insert_id;
            }else{
                $status =500;
                $msg = '保存失败,错误:'.$this->db->getLastError();
            }
        }
        $res['status'] = $status;
        $res['msg'] = $msg;
        $res['id'] = $id;
        return $res;
        
    }
    
    /**
     * 更具id删除数据
     *@param string $table 表名
     * @param int $id 数据id
     * @param string $field 表的字段
     *@return array
     */
    public function del_common($table,$field,$ids){
        if(is_numeric($ids)){
             $id = intval($ids);
            $this->db->where($field, $id);
        }else{
            $id_all = explode(',',$ids);
            $this->db->where($field, $id_all, 'IN');
        }
        
            if($this->db->delete($table)){
                $res['status']=200;
                $res['msg']='删除成功!';
            }else{
                $res['status']=500;
                $res['msg']='删除失败';
            }
        
        return $res;
    }








    public function selectSample() {
        return 'Hello World!';
    }

    public function insertSample($arrInfo) {
        return true;
    }
}
