<?php
//系统设置

class SettingModel extends Db_BaseDb{
    
    private $table = 'setting';
    
    function __construct($status = 'read') {
        parent::__construct($status);
    }
    
    /**
     * 批量修改某个字段
     * @param array $info
     * return array 
     */
    function edit_setting($info){

            if(empty($info)){
                    $status =400;
                    $msg = '参数为空!';
            }else{
                foreach($info as  $k=>$v){
                    $this->db->where ('`key`',$k);
                    $this->db->update ($this->table, array('value'=>$v));
                }
                    $status =200;
                    $msg = '修改成功!';
            }
            $res['status'] = $status;
            $res['msg'] = $msg;
            return $res;   
    }
    
}
