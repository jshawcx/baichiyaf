<?php

class WeixinModel extends Db_BaseDb{
    public function __construct($status = 'read') {
        parent::__construct($status);
	//微信的token号
        $this->token = '';
        $this->AppId='wx0362b675ef005f6b';
        $this->AppSecret='b86d9a7989cc6ef617b22b953066d6c6';
    }

    /*
	*   js获取getSignPackage的接口
	*/
        
    public function getSignPackage() {

			$jsapiTicket = $this->getJsApiTicket();
                    //$url = "http://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI];
                    $tr=$_SERVER['REQUEST_URI'];
                    $url="http://".$_SERVER['HTTP_HOST'].$tr;
                        
			$timestamp = time();
			$nonceStr = $this->createNonceStr();

			// 这里参数的顺序要按照 key 值 ASCII 码升序排序
			$string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";

			$signature = sha1($string);

			$signPackage = array(
			  "appId"     => $this->AppId,
			  "nonceStr"  => $nonceStr,
			  "timestamp" => $timestamp,
			  "url"       => $url,
			  "signature" => $signature,
			  "rawString" => $string
			);
			return $signPackage;
  }

  private function createNonceStr($length = 16) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $str = "";
    for ($i = 0; $i < $length; $i++) {
      $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
    }
    return $str;
  }

  private function getJsApiTicket() {
        //查看数据库有没有记录
        $info = $this->db->getOne('configure', '*');
        if (time()-($info['ticket_time'])>7200 ) {
        $accessToken = $this->GetToken();
        $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=$accessToken";
        $res = json_decode($this->httpGet($url));
        $ticket = $res->ticket;
      if ($ticket) {
            $conf                     = array();
            $conf['ticket_time']      = time();
            $conf['ticket']           = $ticket;
            $this->db->where ('id',1);
            $this->db->update ('configure', $conf);
      }
    } else {
        $ticket = $info['ticket'];
    }
    return $ticket;
  }


  //获取用户的token
  public  function  GetToken(){
                  
                //查看数据库有没有记录
                $info = $this->db->getOne('configure', '*');
		  if($info['AppId'] && $info['AppSecret']){

		         if(time()-($info['access_token_time']) >7200 ||  !($info['access_token'])){

				$getToken = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$info["AppId"].'&secret='.$info["AppSecret"];

			         $token = json_decode(@file_get_contents($getToken),true);

                                         //更新数据库
                                        $conf                     = array();
                                        $conf['access_token_time']      = time();
                                        $conf['access_token']           = isset($token['access_token']) ?  $token['access_token'] : '' ;

                                        $this->db->where ('id',1);
                                        $this->db->update ('configure', $conf);
                                 
					 return  isset($token['access_token']) ?  $token['access_token'] : '';

			}else{
				     return  $info->access_token;
			}
                        
		  }else{

		       $access_token=' ';

		       return  $access_token;
		  }
	}

     //基础授权
     public function getoauth($url,$appid,$type=1){
    	    //回调基础授权

			$returnurl = urlencode($url);
			if($type==2){
			     $urls="https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$appid."&redirect_uri=".$returnurl."&response_type=code&scope=snsapi_userinfo&state=wall#wechat_redirect";
			}
			else{
			   $urls ="https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$appid."&redirect_uri=".$returnurl."&response_type=code&scope=snsapi_base&state=one#wechat_redirect";
			}

			header("Location: ".$urls);
			exit;
	}

    public function back($appid,$appsecret){//获取code后，请求以下链接获取access_token和openid
        
                    if( ! isset($_GET['code'])){
				echo '404';exit;
			}
			$url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$appid."&secret=".trim($appsecret)."&code=".trim($_GET['code'])."&grant_type=authorization_code";
			$curl = curl_init(); // 启动一个CURL会话
			            curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
			            // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
			            // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1); // 从证书中检查SSL加
			            curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
                                    curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
			            curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
			            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
			            $output = curl_exec($curl); // 执行操作
			            curl_close($curl); // 关闭CURL会话
			$token = @json_decode($output, true);
			return $token;
    }
    
    
    
    function  post_sj($api_url,$data){
	    $context = array('http' => array('method' => "POST", 'header' => "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) \r\n Accept: */*", 'content' => $data));
	    $stream_context = stream_context_create($context);
	    echo   $ret = @file_get_contents($api_url, FALSE, $stream_context);
	    return json_decode($ret, true);
    }

  private function httpGet($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_TIMEOUT, 500);
    curl_setopt($curl, CURLOPT_URL, $url);

    $res = curl_exec($curl);
    curl_close($curl);

    return $res;
  }

 //授权接口请求
 public   function  getYz(){
     
            $info = $this->db->getOne('configure', '*');
            
	    //来源网址
		$tr=$_SERVER['REQUEST_URI'];
                $yurl="http://".$_SERVER['HTTP_HOST'].$tr;
                //echo $yurl;exit;
		/*
		*   调试
		*/
		if(isset($_GET['ref'])){
                    
                        setcookie('openid',$_GET['ref'], time()+3600*24,"/");
                     
                        //查看数据库有没有记录
                        $this->db->where ('openid',$_GET['ref']);
                        $userinfo = $this->db->getOne ('member_openid','*');
                        
			 if($userinfo){
			    setcookie('openid',$_GET['ref'], time()+3600*24,"/");
                            return   $userinfo;
			 }
		}
		    //如果存在session
	        if(isset($_COOKIE['openid'])){

                        //查看数据库有没有记录
                        $this->db->where ('openid',$_COOKIE['openid']);
                        $userinfo = $this->db->getOne ('member_openid','*');
                        
			if($userinfo){
                            return   $userinfo;
                        }
		}

			//基础授权
			if(!isset($_GET['code'])){
			     $this->getoauth($yurl,$info['AppId']);
                             
			}elseif(isset($_GET['code']) &&  isset($_GET['state']) &&  $_GET['state']=="wall"){//获取更多信息
                            
			      $token=$this->back($info['AppId'],$info['AppSecret']);
                              //print_r($token);
			      $userinfo=$this->base_info($token['access_token'],$token['openid']);
                              
                                  $json                  = array();
                                 
				  $json['openid']        = $token['openid'];
				  $json['nickname']      = $userinfo['nickname'];
				  $json['img']           = $userinfo['headimgurl'];
                                  $json['sex']           = $userinfo['sex'];
				  $insert_id =  $this->db->insert ('member_openid',$json); 
				  
                                setcookie('openid',$token['openid'], time()+3600*24,"/");
				return  $json; 
                                
			}elseif(isset($_GET['code'])){
                            
			    $token=$this->back($info['AppId'],$info['AppSecret']);
                             
                            //查看数据库有没有记录
                            $this->db->where ('openid',$token['openid']);
                            $userinfo = $this->db->getOne ('member_openid','*');
                                
			     //注册用户
                                if($userinfo){
                                     
				     if(empty($userinfo['nickname'])  ||  empty($userinfo['img'])){
                                         
					        $info  = $this->user_info($token['openid']);
                                                
						$update                    = array();
						$update['nickname']        = isset($info['nickname'])?$info['nickname']:'';
					        $update['img']      = isset($info['headimgurl'])?$info['headimgurl']:'';
                                                
                                                $this->db->where('openid',$token['openid']);
					        $this->db->update("member_openid",$update);
                                    }else{
					        $update['weixin_name']        = $userinfo['nickname'];
					        $update['weixin_img']      = $userinfo['img'];
                                    }
                                    
					  $json                  = array();
					  $json['userid']        = $userinfo['userid'];
					  $json['openid']        = $token['openid'];
					  $json['nickname']      = $update['weixin_name'];
					  $json['img']           = $update['weixin_img'];
                                          
					  setcookie('openid',$token['openid'], time()+3600*24,"/");
					  return    $json; 
                                          
				 }else{
				     //详细信息授权(弹窗)					 
                                    $this->getoauth($yurl,$info['AppId'],2);
				 }
                        }
 }
 
   //获取用户的详细信息
   function  user_info($openid){//通用的token
			$token        = $this->GetToken();
			$url          = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=".$token."&openid=".$openid."&lang=zh_CN";
			$output       = $this->httpGet($url);
			$jsoninfo     = @json_decode($output, true);
            return   $jsoninfo;
   }

   //通过基础信息授权
   function  base_info($access_token,$openid){//网页授权的token
          //求出详细信息
		 $get = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$access_token.'&openid='.$openid.'&lang=zh_CN';
		 $tokens=$this->httpGet($get);
		 $tokens = json_decode($tokens,true);
		 return  $tokens;
   }

}
