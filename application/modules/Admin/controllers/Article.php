<?php

class ArticleController extends Base_AdminCommon{

	public function init(){
		parent::init();
                
            }

/*
 * 资讯列表
 */    
public  function indexAction(){
    $data= array();
    $this->getView()->assign('res',$data);
}


/*
*ajax获取资讯列表
*/

public function article_listAction(){
            //print_r($_GET);exit;
            $draw = isset($_GET['draw']) ? (is_numeric($_GET['draw'])  ?  intval($_GET['draw']):1) : 1;//请求次数计数器
            $page = isset($_GET['start']) ? (is_numeric($_GET['start'])  ?  intval($_GET['start']):0) : 0;//第一条数据的起始位置，比如0代表第一条数据 
            $pagesize = isset($_GET['length']) ? (is_numeric($_GET['length'])  ?  intval($_GET['length']):10) : 10;//告诉服务器每页显示的条数
            $search = isset($_GET['search']) ?trim($_GET['search']['value']):'';//全局的搜索条件
            $cateid = isset($_GET['catid']) ? (is_numeric($_GET['catid'])  ?  intval($_GET['catid']):0) : 0;//分类id
            
            $search_where = array();
            
            $table='article as ar';
            $cols = Array ( "ar.id", "ar.title","ar.catid","ar.updatetime","ar.status","ar.listorder","cates.catname");
            
            if($search!=''){
                $search_where[] = "ar.title like '%".$search."%'";
            }
            //如果有自定义的 搜索
            if(isset($_GET['time_start']) && $_GET['time_start']!=''){
                $search_where[] = " ar.updatetime > '".  strtotime($_GET['time_start'])."' ";
            }
            if(isset($_GET['time_end']) && $_GET['time_end']!=''){
                $search_where[] = " ar.updatetime < '".  strtotime($_GET['time_end'])."' ";
            }
            $search_where[] = " ar.catid = ".$cateid;
           
            $limit = array($page,$pagesize);
            $orderby = array('ar.listorder'=>'asc','ar.id'=>'desc');
            
            $join = array("category as cates", "ar.catid = cates.catid","left");
            
            $sample = new SampleModel();
            echo $sample->list_common($draw,$table,$cols,$search_where,$limit,$orderby,$join);
}

/*
 *上架资讯
 */
public function article_startAction(){
    
    if(!isset($_POST['id'])){
        $res['status'] = 400;
        $res['msg']='参数错误!';
        echo json_encode($res);exit;
    }
    
    $id = intval($_POST['id']);
    $article = new AdminArticleModel();
    $res = $article->article_status($id,'start');
    echo $res;
    
}

/*
 *下架资讯
 */
public function article_stopAction(){
    
    if(!isset($_POST['id'])){
        $res['status'] = 400;
        $res['msg']='参数错误!';
        echo json_encode($res);exit;
    }
    
    $id = intval($_POST['id']);
    $article = new AdminArticleModel();
    $res = $article->article_status($id,'stop');
    echo $res;
    
}

/*
 * 删除资讯
 */

public function article_delAction(){
    if(!isset($_POST['id'])){
        $res['status'] = 400;
        $res['msg']='参数错误!';
        echo json_encode($res);exit;
    }
    
    $table = 'article';
    $field ='id';
    $id = $_POST['id'];
    $sample = new SampleModel();
    $res = $sample->del_common($table,$field,$id);
    $table_content='article_data';
    $res = $sample->del_common($table_content,$field,$id);
    echo json_encode($res);
    
}

/*
 * 添加资讯
 */
public function articleAddAction(){
    $cateid = $this->getRequest()->getParam('cateid');
    $data= array();
    $this->getView()->assign('cateid',$cateid);
    $this->getView()->assign('res',$data);
}

/*
 * 编辑资讯
 */
public function articleEditAction(){
    
    $view_path = $this->getView()->getScriptPath();//:/application/modules/Admin\views
    
    $id = $this->getRequest()->getParam('id');
    $cateid = $this->getRequest()->getParam('cateid');
    
    $data= array();
    $table ='article as ar';
    $field ='ar.id';
    $cols =array("ar.*","ar_data.content");
    $join = array("article_data as ar_data", "ar.id = ar_data.id","left");
    $sample = new SampleModel();
    $data = $sample->info_common($table,$cols,$field,$id,$join);
    
    $this->getView()->assign('cateid',$cateid);
    $this->getView()->assign('res',$data);
    $this->getView()->display('article/articleAdd.phtml');
}



/*
 * 提交资讯
 */
public function articlePostAction(){
   //print_r($this->get_post());exit;
    Helper::import('image');
    
    $info = array();
    $info['title'] = $this->get_post('title');//标题
    $info['catid'] = $this->get_post('cateid');//分类栏目
    $info['listorder'] = intval($this->get_post('sort'));//排序值
    $info['keywords'] = $this->get_post('key_word');//关键词
    $info['description'] = $this->get_post('article_des');//文章摘要
    $info['username'] = $this->get_post('author');//作者
    $info['url'] = $this->get_post('article_sources');//来源
    
    $inputtime = strtotime($this->get_post('datemin'));
    $updatetime = strtotime($this->get_post('datemax'));
   
    $info['inputtime'] = $inputtime;//开始评论时间
    $info['updatetime'] = $updatetime;//结束评论时间
    $img = image_save($this->get_post('file_img',FALSE),'article');//生成并保存图片
    $info['thumb'] = $img;//缩略图
    
    $sample = new SampleModel($status = 'write');
    $table='article';
    $field ='id';
    $id = intval($this->get_post('id'));//编辑的资讯id.新增默认:0
 
    $res_info_one = $sample->add_edit_common($table,$field,$id,$info);
    
    $table='article_data';
    if($id==0){
        $id_status='new';
    }else{
        $id_status='';
    }
    $id = $res_info_one['id'];//编辑的id或新文章的id
    $info_content['id'] =$id;
    $info_content['content'] =  $this->get_post('content',FALSE);//内容
    $res_info = $sample->add_edit_common($table,$field,$id,$info_content,$id_status);
    
    
    echo json_encode($res_info);exit;
    
}






//分类和类型
    function catetypeAction(){

    }
 
 //ajax 获取分类
	public function categoryListAction(){
        $page = 0;
        $pagesize = 1000;
        
        $table='category';
        $cols = Array ("`catid` as id","`parentid` as parent_id","arrparentid","child","arrchildid","`catname` as name","`description` as des","`listorder` as sort","ismenu");
    
        $limit = array($page,$pagesize);
        $orderby = array('listorder'=>'asc','catid'=>'desc');
        
        $search_where = array();
        $search_where[] = " parentdir != '/' ";
        
        $sample = new SampleModel();
        $res= $sample->list_common_normal($table,$cols,$limit,$orderby,$search_where);
       
        echo json_encode($res);exit;
        
	}
        //删除分类
        public function categoryDelAction(){
            //查询是否有子分类,查询是否有资讯在用
            $sample = new SampleModel();
            $table='category';
            $cols='catid';
            $field='parentid';
            $id = $this->get_post('id');
            $res_category = $sample -> info_common($table,$cols,$field,$id);
            if(!empty($res_category)){
		$result['status']=500;
		$result['msg']='不能删除有子分类!';
		echo json_encode($result);exit;
            }
            
            $table = 'article';
            $cols = 'id';
            $field='catid';
            $res_info_article = $sample->info_common($table, $cols, $field, $id);
            if (!empty($res_info_article)) {
                $result['status'] = 500;
                $result['msg'] = '不能删除,该分类下有资讯!';
                echo json_encode($result);
                exit;
            }
            $table = 'category';
            $res_del = $sample->del_common($table, $field, $id);
            if($res_del['status']==200){
                $result['status']=200;
                $result['msg']='删除分类成功!';
                echo json_encode($result);exit;
            }elseif($res_del['status']==500){
                $result['status']=500;
                $result['msg']='删除分类失败!';
                echo json_encode($result);exit;
            }

        }
        //提交分类
	public function categoryPostAction(){
	   //print_r($this->get_post());exit;
	    $info = array();
	    $info['catname'] = $this->get_post('name');//标题
	    $info['description'] = $this->get_post('des');//描述
	    $info['listorder'] = (int)$this->get_post('sort');//排序
	    if($this->get_post('status')){//添加子分类
		    //目前只支持二级分类
		    $product = new SampleModel();
		    $table='category';
		    $cols='parentid';
                    $field='catid';
		    $id = $this->get_post('id');//将要作为父分类的id号
		    $res_info = $product->info_common($table,$cols,$field,$id);
		    if(!empty($res_info)){
		    	if($res_info['parentid']>0){
			    	$result['status']=500;
			    	$result['msg']='目前只支持二级分类!';
			    	echo json_encode($result);exit;
		    	}
                        //查询表xw_article是否有用该分类如果有则分类不能有子分类
                        $table='article';
                        $cols='id';
                        $res_info_article = $product->info_common($table,$cols,$field,$id);
                        if(!empty($res_info_article)){
                            $result['status']=500;
			    $result['msg']='不能作为父分类,该分类下有资讯!';
			    echo json_encode($result);exit;
                        }
		    }else{
		    	$result['status']=500;
		    	$result['msg']='父级分类查询有误!';
		    	echo json_encode($result);exit;
		    }
	    	$info['parentid'] = intval($this->get_post('id'));
	    	$id=0;
	    }else{//编辑或是根分类
	    	$id = intval($this->get_post('id'));//编辑id或新增默认:0
	    }

	    $sample = new SampleModel($status = 'write');
	    $table='category';
            $field='catid';
	    $res_info_one = $sample->add_edit_common($table,$field,$id,$info);
            
	    echo json_encode($res_info_one);exit;


	}

















}