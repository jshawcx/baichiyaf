<?php
//banner管理
class BannerController extends Base_AdminCommon{
    public function init(){
        parent::init();
    }
    
    
    function indexAction(){
        $data = array();
        $this->getView()->assign('res',$data);
    }
    

/*
*ajax获取资讯列表
*/

public function banner_listAction(){
            //print_r($_GET);exit;
            $draw = isset($_GET['draw']) ? (is_numeric($_GET['draw'])  ?  intval($_GET['draw']):1) : 1;//请求次数计数器
            $page = isset($_GET['start']) ? (is_numeric($_GET['start'])  ?  intval($_GET['start']):0) : 0;//第一条数据的起始位置，比如0代表第一条数据 
            $pagesize = isset($_GET['length']) ? (is_numeric($_GET['length'])  ?  intval($_GET['length']):10) : 10;//告诉服务器每页显示的条数
            $search = isset($_GET['search']) ?trim($_GET['search']['value']):'';//全局的搜索条件
            $search_where = array();
            
            $table='focus';
            $cols = Array ( "id","title","`image`","`starttime`","endtime","url","basic");
            
            if($search!=''){
                $search_where[] = "title like '%".$search."%'";
            }
            //如果有自定义的 搜索
            if(isset($_GET['time_start']) && $_GET['time_start']!=''){
                $search_where[] = " starttime > '".  strtotime($_GET['time_start'])."' ";
            }
            if(isset($_GET['time_end']) && $_GET['time_end']!=''){
                $search_where[] = " endtime < '".  strtotime($_GET['time_end'])."' ";
            }
            
            $limit = array($page,$pagesize);
            $orderby = array('listorder'=>'asc','id'=>'desc');
            
            $sample = new SampleModel();
            echo $sample->list_common($draw,$table,$cols,$search_where,$limit,$orderby);
}

/*
 * 删除
 */

public function banner_delAction(){
    if(!isset($_POST['id'])){
        $res['status'] = 400;
        $res['msg']='参数错误!';
        echo json_encode($res);exit;
    }
    
    $table = 'focus';
    $field ='id';
    $id = $_POST['id'];
    $sample = new SampleModel();
    $res = $sample->del_common($table,$field,$id);
    echo json_encode($res);
    
}

/*
 * 添加资讯
 */
public function bannerAddAction(){
    $data= array();
    $this->getView()->assign('res',$data);
}

/*
 * 编辑资讯
 */
public function bannerEditAction(){
    
    $view_path = $this->getView()->getScriptPath();//:/application/modules/Admin\views
    
    $id = $this->getRequest()->getParam('id');
    $data= array();
    $table ='focus';
    $field ='id';
    $cols = "*";
    
    $sample = new SampleModel();
    $data = $sample->info_common($table,$cols,$field,$id);
    
    $this->getView()->assign('res',$data);
    $this->getView()->display('banner/bannerAdd.phtml');
}



/*
 * 提交资讯
 */
public function bannerPostAction(){
   //print_r($this->get_post());
    Helper::import('image');
    
    $info = array();
    $info['title'] = $this->get_post('title');//标题
    $info['listorder'] = intval($this->get_post('sort'));//排序值
    $info['basic'] = intval($this->get_post('basic'));//是否1pc2app
    $info['url'] = $this->get_post('article_sources');//url
    
    $inputtime = strtotime($this->get_post('datemin'));
    $updatetime = strtotime($this->get_post('datemax'));
   
    $info['starttime'] = $inputtime;//开始时间
    $info['endtime'] = $updatetime;//结束时间
    $info['updatetime'] = time();
    
    $img = image_save($this->get_post('file_img',FALSE),'banner');//生成并保存图片
    $info['image'] = $img;//缩略图
    
    $sample = new SampleModel($status = 'write');
    $table='focus';
    $field ='id';
    $id = intval($this->get_post('id'));//编辑的资讯id.新增默认:0
    $res_info = $sample->add_edit_common($table,$field,$id,$info);
   
    echo json_encode($res_info);exit;
    
}

    
}