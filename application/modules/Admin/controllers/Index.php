<?php


class IndexController extends Base_WebCommon{
    public  function init(){
        parent::init();
    }
    
/*
 * 后台首页
 */    
    public function indexAction(){
            //$uid = $this->getRequest()->getParam('uid',0);
            $data= array();
            
            $yac = new Yac();
            $login_count = $yac->get('login_count');
           
            if($login_count){
                $data['count']=$login_count['count'];
                $data['last_ip']=$login_count['last_ip'];
                $data['last_time']=$login_count['last_time'];
                $data['account']=$login_count['account'];
            }else{
                $data['count']='未知';
                $data['last_ip']='未知';
                $data['last_time']='未知';
                $data['account']='未知';
            }
           
            $this->getView()->assign('res',$data);
    }


}