<?php
//会员模块
class MemberController extends Base_AdminCommon{
    
    public function init(){
        parent::init();
    }
    
    /*
    *ajax获取会员列表
    */

    public function member_listAction(){
            //print_r($_GET);exit;
            $draw = isset($_GET['draw']) ? (is_numeric($_GET['draw'])  ?  intval($_GET['draw']):1) : 1;//请求次数计数器
            $page = isset($_GET['start']) ? (is_numeric($_GET['start'])  ?  intval($_GET['start']):0) : 0;//第一条数据的起始位置，比如0代表第一条数据 
            $pagesize = isset($_GET['length']) ? (is_numeric($_GET['length'])  ?  intval($_GET['length']):10) : 10;//告诉服务器每页显示的条数
            $search = isset($_GET['search']) ?trim($_GET['search']['value']):'';//全局的搜索条件
            
            $search_where = array();
            
            $table='member as mb';
            $cols = Array ( "mb.userid", "mb.nickname","mb.money","mb.phone","mb.regdate","mb.islock","groups.name");
            
            if($search!=''){
                $search_where[] = "mb.nickname like '%".$search."%'";
            }
            //如果有自定义的 搜索
            if(isset($_GET['time_start']) && $_GET['time_start']!=''){
                $search_where[] = " mb.regdate > '".  strtotime($_GET['time_start'])."' ";
            }
            if(isset($_GET['time_end']) && $_GET['time_end']!=''){
                $search_where[] = " mb.regdate < '".  strtotime($_GET['time_end'])."' ";
            }
            if(isset($_GET['phone']) && $_GET['phone']!=''){
                $search_where[] = " mb.phone ='".$_GET['phone']."' ";
            }
            
            $search_where[] = " mb.modelid = 1 ";
            
            $limit = array($page,$pagesize);
            $orderby = array('mb.userid'=>'asc');
            
            $join = array("member_group as groups", "mb.groupid = groups.groupid","left");
            
            $sample = new SampleModel();
            echo $sample->list_common($draw,$table,$cols,$search_where,$limit,$orderby,$join);
            
    }

    //会员等级
    public  function memberCateAction(){
        $data= array();
        $this->getView()->assign('res',$data);
    }
    
    
    //会员列表
    public  function memberListAction(){
        $data= array();
        $this->getView()->assign('res',$data);
    }

    //会员详情
    function showUserInfoAction($userid){
        $sample = new SampleModel();
        
        $table ='member';
        $field ='userid';
        $cols ='*';
        $userinfo = $sample->info_common($table,$cols,$field,$userid);
        
        $search_where[]='userid = '.$userid;
        $search_where[]='type = "identity" ';
        $attesta_identity_info = $sample->info_common_where('member_attesta', '*', $search_where);
        if(!empty($attesta_identity_info) && $attesta_identity_info['status']==1){
            $search_where_ali[]='userid = '.$userid;
            $search_where_ali[]='type = "alipay" ';
            $attesta_alipay_info = $sample->info_common_where('member_attesta', '*', $search_where_ali);
            if(!empty($attesta_alipay)){
                $attesta_alipay_arr = string2array($attesta_alipay_info['infos']);
                $attesta_alipay = $attesta_alipay_arr['alipay_account'];
            }else{
                $attesta_alipay = '';
            }
        }else{
            $attesta_alipay = '';
        }
        if(empty($attesta_identity_info)){
            $attesta_identity='';
        }else{
            $attesta_identity_arr = string2array($attesta_identity_info['infos']);
            $attesta_identity = $attesta_identity_arr['id_number'];
        }
        $orderby_order=array('id'=>'asc');;
        $search_where_order[]='buyer_id ='.$userid;
        $orderinfo = $sample->list_common_normal('order','*',null,$orderby_order,$search_where_order);

        $order_zero=0;$order_two=0;$order_three=0;$order_four=0;$order_six=0;$order_seven=0;       
        if(!empty($orderinfo)){
           foreach($orderinfo as $k=>$v){
               if($v['status']==0){
                   $order_zero+=1;
               }elseif($v['status']==2){
                   $order_two+=1;
               }elseif($v['status']==3){
                   $order_three+=1;
               }elseif($v['status']==4){
                   $order_four+=1;
               }elseif($v['status']==6){
                   $order_six+=1;
               }elseif($v['status']==7){
                   $order_seven+=1;
               }
           } 
        }
        $order_array = array();
        $order_array['zero']=$order_zero;$order_array['two']=$order_two;$order_array['three']=$order_three;
        $order_array['four']=$order_four;$order_array['six']=$order_six;$order_array['seven']=$order_seven;
       
        $this->getView()->assign('res',$userinfo);
        $this->getView()->assign('attesta_identity',$attesta_identity);
        $this->getView()->assign('attesta_alipay',$attesta_alipay);
        $this->getView()->assign('order_count',$order_array);
        
    }
    /*
     * 修改密码view
     */
    function changePasswordAction($userid){
        $data= array();
        $table ='member';
        $field ='userid';
        $cols =array('userid','nickname','encrypt','password');
        $sample = new SampleModel();
        $data = $sample->info_common($table,$cols,$field,$userid);
        $this->getView()->assign('res',$data);
    }
    
    /*
     *提交修改密码
     */
    function editPasswordAction(){
        $userid = $this->get_post('userid');
        $encrypt = $this->get_post('encrypt');
        $password = $this->get_post('new-password');
        $sample = new SampleModel($status = 'write');
        $table='member';
        $field ='userid';
        $id = intval($userid);//会员id
        $info['password'] = md5(md5($password.$encrypt));
        $res_info = $sample->add_edit_common($table,$field,$id,$info);
        echo json_encode($res_info);exit;
    }
    /*
     *启用
     */
    public function member_startAction(){
        if(!isset($_POST['id'])){
            $res['status'] = 400;
            $res['msg']='参数错误!';
            echo json_encode($res);exit;
        }
        $id = intval($_POST['id']);
        $member = new AdminMemberModel();
        $res = $member->member_status($id,'start');
        echo $res;
    }

    /*
     *关闭
     */
    public function member_stopAction(){
        if(!isset($_POST['id'])){
            $res['status'] = 400;
            $res['msg']='参数错误!';
            echo json_encode($res);exit;
        }

        $id = intval($_POST['id']);
        $member = new AdminMemberModel();
        $res = $member->member_status($id,'stop');
        echo $res;
    }
    //会员资金明细
    function memberMoneyInfoAction($userid){
        echo $userid;
    }
    
    
    
    
} 

