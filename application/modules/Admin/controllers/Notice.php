<?php

class NoticeController extends Base_AdminCommon{

	public function init(){
		parent::init();
	}

/*
 * 公告列表
 */    
public  function indexAction(){
    $data= array();
    $this->getView()->assign('res',$data);
}


/*
*ajax获取资讯列表
*/

public function notice_listAction(){
            //print_r($_GET);exit;
            $draw = isset($_GET['draw']) ? (is_numeric($_GET['draw'])  ?  intval($_GET['draw']):1) : 1;//请求次数计数器
            $page = isset($_GET['start']) ? (is_numeric($_GET['start'])  ?  intval($_GET['start']):0) : 0;//第一条数据的起始位置，比如0代表第一条数据 
            $pagesize = isset($_GET['length']) ? (is_numeric($_GET['length'])  ?  intval($_GET['length']):10) : 10;//告诉服务器每页显示的条数
            $search = isset($_GET['search']) ?trim($_GET['search']['value']):'';//全局的搜索条件
            
            $search_where = array();
            
            $table='announce';
            $cols = Array ( "announceid", "title","starttime","endtime","inputtime","listorder","passed");
            
            if($search!=''){
                $search_where[] = "title like '%".$search."%'";
            }
            //如果有自定义的 搜索
            if(isset($_GET['time_start']) && $_GET['time_start']!=''){
                $search_where[] = " starttime > '".  strtotime($_GET['time_start'])."' ";
            }
            if(isset($_GET['time_end']) && $_GET['time_end']!=''){
                $search_where[] = " endtime < '".  strtotime($_GET['time_end'])."' ";
            }
            
            $limit = array($page,$pagesize);
            $orderby = array('listorder'=>'asc','announceid'=>'desc');
            
            $sample = new SampleModel();
            echo $sample->list_common($draw,$table,$cols,$search_where,$limit,$orderby);
}

/*
 *上架资讯
 */
public function notice_startAction(){
    
    if(!isset($_POST['id'])){
        $res['status'] = 400;
        $res['msg']='参数错误!';
        echo json_encode($res);exit;
    }
    
    $id = intval($_POST['id']);
    $article = new AdminNoticeModel();
    $res = $article->article_status($id,'start');
    echo $res;
    
}

/*
 *下架资讯
 */
public function notice_stopAction(){
    
    if(!isset($_POST['id'])){
        $res['status'] = 400;
        $res['msg']='参数错误!';
        echo json_encode($res);exit;
    }
    
    $id = intval($_POST['id']);
    $article = new AdminNoticeModel();
    $res = $article->article_status($id,'stop');
    echo $res;
    
}

/*
 * 删除资讯
 */

public function notice_delAction(){
    if(!isset($_POST['id'])){
        $res['status'] = 400;
        $res['msg']='参数错误!';
        echo json_encode($res);exit;
    }
    
    $table = 'announce';
    $field ='announceid';
    $id = $_POST['id'];
    $sample = new SampleModel();
    $res = $sample->del_common($table,$field,$id);
    echo json_encode($res);
    
}

/*
 * 添加资讯
 */
public function noticeAddAction(){
    $data= array();
    $this->getView()->assign('res',$data);
}

/*
 * 编辑资讯
 */
public function noticeEditAction(){
    
    $view_path = $this->getView()->getScriptPath();//:/application/modules/Admin\views
    
    $id = $this->getRequest()->getParam('id');
    $data= array();
    $table ='announce';
    $field ='announceid';
    $cols ='*';
    $sample = new SampleModel();
    $data = $sample->info_common($table,$cols,$field,$id);
   
    $this->getView()->assign('res',$data);
    $this->getView()->display('notice/noticeAdd.phtml');
}



/*
 * 提交资讯
 */
public function noticePostAction(){
   //print_r($this->get_post());
   // Helper::import('image');
    
    $info = array();
    $info['title'] = $this->get_post('title');//标题
    $info['listorder'] = intval($this->get_post('sort'));//排序值
    
    $datamin = strtotime($this->get_post('datemin'));
    $datamax = strtotime($this->get_post('datemax'));
    
    $info['starttime'] = $datamin;//开始评论时间
    $info['endtime'] = $datamax;//结束评论时间
    
    $info['content'] =  $this->get_post('content',FALSE);//内容
    
    $sample = new SampleModel($status = 'write');
    $table='announce';
    $field ='announceid';
    $id = intval($this->get_post('id'));//编辑的资讯id.新增默认:0
    $res_info = $sample->add_edit_common($table,$field,$id,$info);

    echo json_encode($res_info);exit;
    
}

















}