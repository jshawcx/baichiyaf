<?php

class ProductController extends Base_AdminCommon{

	public function init(){
		parent::init();
	}

//品牌
	public function brandAction(){
		echo '品牌';exit;
	}


	//提交品牌

//分类
	public function categoryAction(){
		//
	}

//ajax 获取分类
	public function categoryListAction(){
        $page = 0;
        $pagesize = 1000;
        
        $table='product_category';
        $cols = Array ( "id", "parent_id","name","des","sort");
        
        $limit = array($page,$pagesize);
        $orderby = array('sort'=>'asc','id'=>'desc');
        
        $sample = new SampleModel();
        $res= $sample->list_common_normal($table,$cols,$limit,$orderby);
        echo json_encode($res);exit;
	}


	//提交分类
	public function categoryPostAction(){
	   //print_r($this->get_post());exit;
	    
	    $info = array();
	    $info['name'] = $this->get_post('name');//标题
	    $info['des'] = $this->get_post('des');//描述
	    $info['sort'] = (int)$this->get_post('sort');//排序
	    
	    if($this->get_post('status')){

		    //目前只支持二级分类
		    $product = new SampleModel();
		    $table='product_category';
		    $cols='parent_id';
		    $id = $this->get_post('id');
		    $res_info = $product->info_common($table,$cols,$id);
		    if(!empty($res_info)){
		    	if($res_info['parent_id']>0){
			    	$result['status']=500;
			    	$result['msg']='目前只支持二级分类!';
			    	echo json_encode($result);exit;
		    	}
		    }else{
		    	$result['status']=500;
		    	$result['msg']='父级分类查询有误!';
		    	echo json_encode($result);exit;
		    }

	    	$info['parent_id'] = intval($this->get_post('id'));
	    	$id ='';
	    }else{
	    	$id = intval($this->get_post('id'));//编辑的资讯id.新增默认:0
	    }

	    $sample = new SampleModel($status = 'write');
	    $table='product_category';
	    $res_info = $sample->add_edit_common($table,$id,$info);

	    echo json_encode($res_info);exit;


	}

//产品列表
	public function indexAction(){
		echo '产品列表';exit;
	}

	//提交产品

}