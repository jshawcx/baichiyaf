<?php

class SayController extends Base_AdminCommon{

	public function init(){
		parent::init();
	}

/*
 * 资讯列表
 */    
public  function indexAction(){
    $data= array();
    $this->getView()->assign('res',$data);
}


/*
*ajax获取资讯列表
*/

public function say_listAction(){
            //print_r($_GET);exit;
            $draw = isset($_GET['draw']) ? (is_numeric($_GET['draw'])  ?  intval($_GET['draw']):1) : 1;//请求次数计数器
            $page = isset($_GET['start']) ? (is_numeric($_GET['start'])  ?  intval($_GET['start']):0) : 0;//第一条数据的起始位置，比如0代表第一条数据 
            $pagesize = isset($_GET['length']) ? (is_numeric($_GET['length'])  ?  intval($_GET['length']):10) : 10;//告诉服务器每页显示的条数
            $search = isset($_GET['search']) ?trim($_GET['search']['value']):'';//全局的搜索条件
            $search_where = array();
            
            $table='gonglue';
            $cols = Array ( "id","title","`like`","`read`","shows_index","updatetime");
            
            if($search!=''){
                $search_where[] = "title like '%".$search."%'";
            }
            //如果有自定义的 搜索
            if(isset($_GET['time_start']) && $_GET['time_start']!=''){
                $search_where[] = " updatetime > '".  strtotime($_GET['time_start'])."' ";
            }
            if(isset($_GET['time_end']) && $_GET['time_end']!=''){
                $search_where[] = " updatetime < '".  strtotime($_GET['time_end'])."' ";
            }
            
            $limit = array($page,$pagesize);
            $orderby = array('listorder'=>'asc','id'=>'desc');
            
            $sample = new SampleModel();
            echo $sample->list_common($draw,$table,$cols,$search_where,$limit,$orderby);
}

/*
 *上架资讯
 */
public function say_startAction(){
    
    if(!isset($_POST['id'])){
        $res['status'] = 400;
        $res['msg']='参数错误!';
        echo json_encode($res);exit;
    }
    
    $id = intval($_POST['id']);
    $article = new AdminSayModel();
    $res = $article->say_status($id,'start');
    echo $res;
    
}

/*
 *下架资讯
 */
public function say_stopAction(){
    
    if(!isset($_POST['id'])){
        $res['status'] = 400;
        $res['msg']='参数错误!';
        echo json_encode($res);exit;
    }
    
    $id = intval($_POST['id']);
    $article = new AdminSayModel();
    $res = $article->say_status($id,'stop');
    echo $res;
    
}

/*
 * 删除资讯
 */

public function say_delAction(){
    if(!isset($_POST['id'])){
        $res['status'] = 400;
        $res['msg']='参数错误!';
        echo json_encode($res);exit;
    }
    
    $table = 'gonglue';
    $field ='id';
    $id = $_POST['id'];
    $sample = new SampleModel();
    $res = $sample->del_common($table,$field,$id);
    $table_content='gonglue_data';
    $res = $sample->del_common($table_content,$field,$id);
    echo json_encode($res);
    
}

/*
 * 添加资讯
 */
public function sayAddAction(){
    $data= array();
    $this->getView()->assign('res',$data);
}

/*
 * 编辑资讯
 */
public function sayEditAction(){
    
    $view_path = $this->getView()->getScriptPath();//:/application/modules/Admin\views
    
    $id = $this->getRequest()->getParam('id');
    $data= array();
    $table ='gonglue as lue';
    $field ='lue.id';
    $cols = array("lue.*","gongluecon.content");
    $join = array("gonglue_data as gongluecon", "lue.id = gongluecon.id","left");
    
    $sample = new SampleModel();
    $data = $sample->info_common($table,$cols,$field,$id,$join);
    
    $this->getView()->assign('res',$data);
    $this->getView()->display('say/sayAdd.phtml');
}



/*
 * 提交资讯
 */
public function sayPostAction(){
   //print_r($this->get_post());
    Helper::import('image');
    
    $info = array();
    $info['title'] = $this->get_post('title');//标题
    $info['description'] = $this->get_post('article_des');//文章摘要
    $info['listorder'] = intval($this->get_post('sort'));//排序值
    $info['shows_index'] = intval($this->get_post('review'));//是否隐藏0.1显示
    
    $info['inputtime'] = time();//更新时间
    $info['updatetime'] = time();//修改时间
    
    $img = image_save($this->get_post('file_img',FALSE),'say');//生成并保存图片
    $info['thumb'] = $img;//缩略图
    
    $sample = new SampleModel($status = 'write');
    $table='gonglue';
    $field ='id';
    $id = intval($this->get_post('id'));//编辑的资讯id.新增默认:0
    $res_info_one = $sample->add_edit_common($table,$field,$id,$info);
    
    
    $table='gonglue_data';
    $field ='id';
    if($id==0){
        $id_status='new';
    }else{
        $id_status='';
    }
    $id = $res_info_one['id'];//编辑的id或新文章的id
    $info_content['id'] =$id;
    $info_content['content'] =  $this->get_post('content',FALSE);//内容
    $res_info = $sample->add_edit_common($table,$field,$id,$info_content,$id_status);
    
    
    echo json_encode($res_info);exit;
    
}

















}