<?php
//系统设置
class SettingController extends Base_AdminCommon{
    public function init(){
        parent::init();
    }
    
    function indexAction(){
        $page = 0;
        $pagesize = 500;
        
        $table='setting';
        $cols = "*";
        
        $limit = array($page,$pagesize);
        $orderby = array('listorder'=>'asc');
        
        $sample = new SampleModel();
        $data = $sample->list_common_normal($table,$cols,$limit,$orderby);
        foreach($data as $k=>$v){
            $system_info[$v['key']]=$v['value'];
        }
        $this->getView()->assign('res',$system_info);
    }
    
    
    
    function settinPostOneAction(){
        Helper::import('image');
        $logo_zhu = image_save($this->get_post('file_img_one',FALSE),'weblogo');//生成并保存图片
        $logo_fu = image_save($this->get_post('file_img_two',FALSE),'weblogo');//副logo
        
        $info = array();
        $info['webname'] = $this->get_post('website_name');//网站名称
        $info['site_web_title'] = $this->get_post('website_title');//网站标题
        $info['keyword'] = $this->get_post('website_Keywords');//关键词
        $info['description'] = $this->get_post('website_des');//描述
        $info['site_web_copyright'] = $this->get_post('website_static');//版权
        $info['site_statistical_code'] = $_POST['website_code'];//统计代码
        $info['site_web_icp'] = $this->get_post('website_icp');//备案号
        $info['site_logo_zhu'] = $logo_zhu;
        $info['site_logo_fu'] = $logo_fu;
        $setting = new SettingModel($status = 'write');
        $res_info = $setting->edit_setting($info);
        echo json_encode($res_info);exit;
    }
    
    
    
    function settinPostTwoAction(){
        Helper::import('image');
        $file_img_weixin = image_save($this->get_post('file_img_weixin',FALSE),'weblogo');
        $file_img_map = image_save($this->get_post('file_img_map',FALSE),'weblogo');
        
        $info = array();
        $info['xinlang'] = $this->get_post('xinlang');
        $info['tengxun'] = $this->get_post('tengxun');
        $info['site_contact_qq1'] = $this->get_post('site_contact_qq1');
        $info['site_complain_qq'] = $this->get_post('site_complain_qq');
        $info['site_collaborate_qq'] = $this->get_post('site_collaborate_qq');
        $info['site_consult_qq'] = $this->get_post('site_consult_qq');
        $info['site_collaborate_qq'] = $this->get_post('site_collaborate_qq');
        $info['site_service_qq'] = $this->get_post('site_service_qq');
        $info['site_contact_tel'] = $this->get_post('site_contact_tel');
        $info['site_contact_email'] = $this->get_post('site_contact_email');
        $info['site_contact_zipcode'] = $this->get_post('site_contact_zipcode');
        $info['site_contact_city'] = $this->get_post('site_contact_city');
        $info['site_work_day'] = $this->get_post('site_work_day');
        $info['site_contact_address'] = $this->get_post('site_contact_address');
        
        $info['weixin_logo'] = $file_img_weixin;
        $info['site_map_image'] = $file_img_map;
        
        $setting = new SettingModel($status = 'write');
        $res_info = $setting->edit_setting($info);
        echo json_encode($res_info);exit;
    }
    
    
    
    function  scoreAction(){
        
    }
    
    
}
