<?php
//获取资讯的接口

class ApiController extends Yaf_Controller_Abstract{
    
    private $pagesize = 10;
    
    public function init(){
	if ($this->getRequest()->isxmlHttpRequest()) {
            Yaf_Dispatcher::getInstance()->disableView();
        }
        Helper::import('YafToSimple');
        Helper::import('Common');
        Helper::import('string');
        Helper::import('SessionCookie');
        Helper::import('Security');
        $this->api = new ApiModel();
    }
    
    //总的回调使用
    function call_back($status=400,$msg='参数错误',$res=array()){
        $result['status'] = $status;
        $result['msg'] =$msg;
        $result['res'] = $res;
        //print_r($result);exit;
        return json_encode($result);
    }
    
    
    
    
    function indexAction(){
        print_r($_GET);
        echo '接口';exit;
    }
    
    function testAction(){
        print_r($_GET);
        echo '接口';exit;
    }
  
    //验证验证码  
    //127.0.0.1/api/api/testYzm/yzm/xxxxx
    function testYzmAction(){
        $params = $this->getRequest()->getParams();
        if(empty($params)  || !array_key_exists('yzm', $params)){
            $status=400;$msg='参数错误!';
            echo $this->call_back($status, $msg);exit;
        }
        if($params['yzm']==''){
            $status=400;$msg='参数不能为空!!';
            echo $this->call_back($status, $msg);exit;
        }
        $yzm =  get_cookie('yzm');
        $yanzm = $params['yzm'];
        
        if($yzm != $yanzm){
            $status=400;$msg='验证码错误..';
            echo $this->call_back($status, $msg);exit;
        }
        $status=200;$msg='验证码正确';
        echo $this->call_back($status, $msg);exit;
    }
    
    //验证码
    //127.0.0.1/api/api/getYzm/phone/18550518980
    function getYzmAction(){
        
        $params = $this->getRequest()->getParams();
        if(empty($params)  || !array_key_exists('phone', $params)){
            $status=400;$msg='参数错误!';
            echo $this->call_back($status, $msg);exit;
        }
        if($params['phone']==''){
            $status=400;$msg='参数不能为空!!';
            echo $this->call_back($status, $msg);exit;
        }
        
        $yzm_time =  get_cookie('yzm_time');
        
        if($yzm_time!='' && time()- $yzm_time < 60){
            $status=400;$msg='请等会再试!!!';
            echo $this->call_back($status, $msg);exit;
        }
        
        $verify_code = rand(100000,999999);
       
        $message='您正在申请注册,激活码【';
        $message.=$verify_code;
        $message.='】，如非本人操作,请致电0512-86888251。';
       
        $url='http://106.ihuyi.cn/webservice/sms.php?method=Submit&account=cf_yamixinxi&password=1234567&mobile='.$params['phone'].'&content='.$message." ";
        
        $res = file_get_contents($url);
        $postObj = simplexml_load_string($res, 'SimpleXMLElement', LIBXML_NOCDATA);
        
        $code = $postObj->code;
        $msg = $postObj->msg;
        if($code == 2){
            set_cookie('yzm', $verify_code,120);
            set_cookie('yzm_time', time(),120);
            $status=200;$yzmcode = $verify_code;
            echo $this->call_back($status, $msg,$yzmcode);exit;
        }else{
            $status=500;
            echo $this->call_back($status, $msg);exit;
        }
    }
    
   //注册
    //127.0.0.1/api/api/register/phone/18550518980/password/111111
    function registerAction(){
        
        $params = $this->getRequest()->getParams();
        if(empty($params)  || !array_key_exists('phone', $params) || !array_key_exists('password', $params)){
            $status=400;$msg='参数错误!';
            echo $this->call_back($status, $msg);exit;
        }
        if($params['phone']==''||$params['password']==''){
            $status=400;$msg='参数不能为空!!';
            echo $this->call_back($status, $msg);exit;
        }
        
        $phone = trim($params['phone']);
        $password = trim($params['password']);
        
        $search_where[]='phone="'.$phone.'"';
        $user = $this->api->info_common('member','userid',$search_where);
        if(!empty($user)){
            $status=500;$msg='手机号已经被使用';
            echo $this->call_back($status, $msg);exit;
        }
        
        $encrypt = random(6);
        $password = md5(md5($password.$encrypt));
        $client_ip = get_client_ip();
        
        $info['modelid'] = 1;
        $info['encrypt'] = $encrypt;
        $info['password'] = $password;
        $info['nickname'] = $phone;
        $info['phone'] =    $phone;
        $info['regdate'] = time();
        $info['regip'] = $client_ip;
        $info['lastdate'] = time();
        $info['lastip'] = $client_ip;
        $info['loginnum'] =1;
        $info['islock'] = 0;
        $info['point'] = (int) 0;
        $info['groupid'] = 1;
        $info['phone_status']=1;
        
       $add = $this->api->add_common('member',$info);
       
       if($add['status']!=200){
            echo $this->call_back($add['status'],$add['msg']);exit;
       }
       $userid = encryption($add['id']);//加密过的userid
       set_cookie('userid', $userid,86400);
       
       //echo get_cookie('userid');
       //echo decryption($userid);//解密
       
       echo $this->call_back(200,'注册成功');exit;
    }
   // 登陆
    //127.0.0.1/api/api/login/phone/18550518980/password/123123
    function loginAction(){
        $params = $this->getRequest()->getParams();
        if(empty($params)  || !array_key_exists('phone', $params) || !array_key_exists('password', $params)){
            $status=400;$msg='参数错误!';
            echo $this->call_back($status, $msg);exit;
        }
        if($params['phone']==''||$params['password']==''){
            $status=400;$msg='参数不能为空!!';
            echo $this->call_back($status, $msg);exit;
        }
        
        $phone = trim($params['phone']);
        $password = trim($params['password']);
        
        $cols = array('userid','nickname','modelid','phone','password','encrypt','islock','loginnum');
        $search_where[]='phone="'.$phone.'"';
        $user = $this->api->info_common('member',$cols,$search_where);
        
        if(empty($user)){
            $status=500;$msg='你还未注册';
            echo $this->call_back($status, $msg);exit;
        }
        if($user['modelid']!=1){
            //$status=500;$msg='商家还不能在移动端登陆!';
            //echo $this->call_back($status, $msg);exit;
        }
        if($user['islock'] == 1){
            $status=500;$msg='你的账号已被冻结.';
            echo $this->call_back($status, $msg);exit;
        }
        if($user['password'] != md5(md5($password.$user['encrypt']))){
            $status=500;$msg='你的账号密码错误..';
            echo $this->call_back($status, $msg);exit;
        }
        
        //更新 最后一次登录的信息
        $client_ip = get_client_ip();
        $update_info = array(
            'lastdate'=>time(),
            'lastip'=>$client_ip,
            'loginnum'=>$user['loginnum']+1,
        );
        $this->api->update_common('member','userid',$user['userid'],$update_info);
        
        $userid = encryption($user['userid']);
        set_cookie('userid', $userid,86400);
        
       $res_userinfo= array(
           'userid'=>$userid,
           'nickname'=>$user['nickname'],
           'phone'=>$user['phone'],
           'encrypt'=>$user['encrypt'],
           'avatar'=>getavatar($user['userid'])
           );
       $status=200;$msg='获取成功';
       echo $this->call_back($status, $msg,$res_userinfo);exit;

    }
   //修改密码
     //127.0.0.1/api/api/changePassword/uid/xxxxxx/encrypt/xxxxx/oldpass/xxxx/newpass/xxxxx
   function changePasswordAction(){
       
        $params = $this->getRequest()->getParams();
        if(empty($params)  || !array_key_exists('uid', $params) || !array_key_exists('encrypt', $params) || !array_key_exists('oldpass', $params) || !array_key_exists('newpass', $params)){
            $status=400;$msg='参数错误!';
            echo $this->call_back($status, $msg);exit;
        }
        if($params['uid']==''||$params['encrypt']==''||$params['oldpass']==''||$params['newpass']==''){
            $status=400;$msg='参数不能为空!!';
            echo $this->call_back($status, $msg);exit;
        }
        
        $uid = trim($params['uid']);
        $encrypt = trim($params['encrypt']);
        $oldpass = trim($params['oldpass']);
        $newpass = trim($params['newpass']);
        
        $userid = decryption($uid);//解密;
        //echo $userid;
        if(empty($userid)){
            $status = 400;$msg='没有该用户';
            echo $this->call_back($status, $msg);exit;
        }
        $cols = array('password','userid');
        $search_where[]='userid='.$userid;
        $user = $this->api->info_common('member',$cols,$search_where);
        
        if(empty($user)){
            $status=500;$msg='没有该用户';
            echo $this->call_back($status, $msg);exit;
        }
        
        $oldpass = md5(md5($oldpass.$encrypt));
        $newpass = md5(md5($newpass.$encrypt));
        
        if ($oldpass != $user['password']) {
            $status=500;$msg='原密码错误!';
            echo $this->call_back($status, $msg);exit;
        }
        $update_info= array(
            'password'=>$newpass
        );
        $update = $this->api->update_common('member','userid',$userid,$update_info);
        
        echo $this->call_back($update['status'], $update['msg']);exit;
    }
    //忘记密码
    //127.0.0.1/api/api/forgetPassword/phone/xxxxxx/yanzm/xxxxx/newpass/xxxxx
   function forgetPasswordAction(){

        $params = $this->getRequest()->getParams();
        if(empty($params)  || !array_key_exists('phone', $params) || !array_key_exists('yanzm', $params) || !array_key_exists('newpass', $params)){
            $status=400;$msg='参数错误!';
            echo $this->call_back($status, $msg);exit;
        }
        if($params['phone']==''||$params['yanzm']==''||$params['newpass']==''){
            $status=400;$msg='参数不能为空!!';
            echo $this->call_back($status, $msg);exit;
        }
        
        $phone = trim($params['phone']);
        $yanzm = trim($params['yanzm']);
        $newpass = trim($params['newpass']);
        
        $yzm_time =  get_cookie('yzm_time');
        $yzm =  get_cookie('yzm');
        
        if($yzm_time =='' || time()- $yzm_time > 60){
            $status=400;$msg='验证码已经过期.';
            echo $this->call_back($status, $msg);exit;
        }
        
        if($yzm != $yanzm){
            $status=400;$msg='验证码错误..';
            echo $this->call_back($status, $msg);exit;
        }
        
        $cols = array('userid','encrypt');
        $search_where[]='phone="'.$phone.'"';
        $user = $this->api->info_common('member',$cols,$search_where);
        
        if(empty($user)){
            $status=500;$msg='你还未注册';
            echo $this->call_back($status, $msg);exit;
        }
        
        $userid = $user['userid'];
        $encrypt= $user['encrypt'];
        $newpass = md5(md5($newpass.$encrypt));
        $update_info= array(
            'password'=>$newpass
        );
        $update = $this->api->update_common('member','userid',$userid,$update_info);
        
        echo $this->call_back($update['status'],$update['msg']);exit; 
       
   }
   //修改头像
    
   //获取资料信息
   
   //修改资料信息 
   
  //是否实名认证
    //127.0.0.1/api/api/attestName/uid/xxxxxx
   function attestNameAction(){
       
        $params = $this->getRequest()->getParams();
        if(empty($params)  || !array_key_exists('uid', $params)){
            $status=400;$msg='参数错误!';
            echo $this->call_back($status, $msg);exit;
        }
        if($params['uid']==''){
            $status=400;$msg='参数不能为空!!';
            echo $this->call_back($status, $msg);exit;
        }
        
        $uid = trim($params['uid']);
        $userid = decryption($uid);//解密;
        //echo $userid;
        if(empty($userid)){
            $status = 400;$msg='没有该用户';
            echo $this->call_back($status, $msg);exit;
        }
        
        $cols = array('userid','status');
        $search_where[]='userid='.$userid;
        $search_where[]='type="identity"';
        $user = $this->api->info_common('member_attesta',$cols,$search_where);
        
        if(empty($user)){
            $status=500;$msg='还未实名认证';
            echo $this->call_back($status, $msg);exit;
        }
        if($user['status']!=1){
            $status=500;$msg='待审核或审核失败!';
            echo $this->call_back($status, $msg);exit;
        }
        
        $status=200;$msg='通过实名认证';
        echo $this->call_back($status, $msg);exit;
   }
  //提交实名认证  
  //127.0.0.1/api/api/editAttestName/uid/xxxxxx/name/xxxxx/number/xxxxxx
  function editAttestNameAction(){
        $params = $this->getRequest()->getParams();
        
        if(empty($params)  || !array_key_exists('uid', $params) || !array_key_exists('name', $params) || !array_key_exists('number', $params)){
            $status=400;$msg='参数错误!';
            echo $this->call_back($status, $msg);exit;
        }
        if($params['uid']==''||$params['name']==''||$params['number']==''){
            $status=400;$msg='参数不能为空!!';
            echo $this->call_back($status, $msg);exit;
        }
        
        $uid = trim($params['uid']);
        $name = trim($params['name']);
        $number = trim($params['number']);
        
        $name = urldecode($name);

        $userid = decryption($uid);//解密;
        //echo $userid;
        if(empty($userid)){
            $status = 400;$msg='没有该用户';
            echo $this->call_back($status, $msg);exit;
        }
        
        $cols = array('userid','status');
        $search_where[]='userid='.$userid;
        $search_where[]='type="identity"';
        $user = $this->api->info_common('member_attesta',$cols,$search_where);
        
        if(!empty($user) && $user['status']==1){
            $status=500;$msg='你已经认证,不能重复认证';
            echo $this->call_back($status, $msg);exit;
        }
        if(!empty($user) && $user['status']==0){
            $status=500;$msg='认证等待审核中!';
            echo $this->call_back($status, $msg);exit;
        }
        if(!empty($user) && $user['status']==-1){
            $infos = array(
                'name'=>$name,
                'id_number'=>$number
            );
            $infos = array2string($infos);
            $update_info= array(
                'updatetime'=>time(),
                'infos'=> $infos,
                'status'=>0
            );
            $update = $this->api->update_common('member_attesta','userid',$userid,$update_info);
            
            echo $this->call_back($update['status'],$update['msg']);exit;
        }
        if(empty($user)){
            
            $infos = array(
                'name'=>$name,
                'id_number'=>$number
            );
            $infos = array2string($infos);
        
            $add_info= array(
                'userid'=>$userid,
                'dateline'=>time(),
                'infos'=>  $infos,
                'status'=>0,
                'type'=>'identity'
            );
            $add = $this->api->add_common('member_attesta',$add_info);
            echo $this->call_back($add['status'],$add['msg']);exit;
        }
        
  }
  //
  //是否支付宝绑定
    //127.0.0.1/api/api/attestAlipay/uid/xxxxxx
   function attestAlipayAction(){
       
        $params = $this->getRequest()->getParams();
        if(empty($params)  || !array_key_exists('uid', $params)){
            $status=400;$msg='参数错误!';
            echo $this->call_back($status, $msg);exit;
        }
        if($params['uid']==''){
            $status=400;$msg='参数不能为空!!';
            echo $this->call_back($status, $msg);exit;
        }
        
        $uid = trim($params['uid']);
        $userid = decryption($uid);//解密;
        //echo $userid;
        if(empty($userid)){
            $status = 400;$msg='没有该用户';
            echo $this->call_back($status, $msg);exit;
        }
        
        $cols = array('userid','status');
        $search_where[]='userid='.$userid;
        $search_where[]='type="alipay"';
        $user = $this->api->info_common('member_attesta',$cols,$search_where);
        
        if(empty($user)){
            $status=500;$msg='还未绑定';
            echo $this->call_back($status, $msg);exit;
        }

        $status=200;$msg='已经绑定支付宝';
        echo $this->call_back($status, $msg);exit;
   }
  //提交支付宝绑定
  //127.0.0.1/api/api/editAttestAlipay/uid/xxxxxx/number/xxxxxx
  function editAttestAlipayAction(){
      
        $params = $this->getRequest()->getParams();
        if(empty($params)  || !array_key_exists('uid', $params)  || !array_key_exists('number', $params)){
            $status=400;$msg='参数错误!';
            echo $this->call_back($status, $msg);exit;
        }
        if($params['uid']==''|| $params['number']==''){
            $status=400;$msg='参数不能为空!!';
            echo $this->call_back($status, $msg);exit;
        }
        
        $uid = trim($params['uid']);
        $number = trim($params['number']);

        $userid = decryption($uid);//解密;
        //echo $userid;
        if(empty($userid)){
            $status = 400;$msg='没有该用户';
            echo $this->call_back($status, $msg);exit;
        }
        
        $cols = array('userid','status');
        $search_where[]='userid='.$userid;
        $search_where[]='type="identity"';
        $user = $this->api->info_common('member_attesta',$cols,$search_where);
        
        if(empty($user) || $user['status']!=1){
            $status=500;$msg='未实名认证,审核中或认证失败!';
            echo $this->call_back($status, $msg);exit;
        }
       
        $cols_ali = array('userid','status');
        $search_where_ali[]='userid='.$userid;
        $search_where_ali[]='type="alipay"';
        $user_ali = $this->api->info_common('member_attesta',$cols_ali,$search_where_ali);
        
        if(empty($user_ali)){

            $infos = array(
                'alipay_account'=>$number
            );
            $infos = array2string($infos);
        
            $add_info= array(
                'userid'=>$userid,
                'dateline'=>time(),
                'updatetime'=>time(),
                'infos'=>  $infos,
                'status'=>1,
                'type'=>'alipay'
            );
            $add = $this->api->add_common('member_attesta',$add_info);
            echo $this->call_back($add['status'],$add['msg']);exit;
        }else{
            $status=500;$msg='你已经绑定,不能重复绑定';
            echo $this->call_back($status, $msg);exit;
        }
        
  }
  //申请提现  
  //127.0.0.1/api/api/withdrawMoney/uid/xxxxxx/money/xxxxxx
  function withdrawMoneyAction(){
        $params = $this->getRequest()->getParams();
        if(empty($params)  || !array_key_exists('uid', $params)  || !array_key_exists('money', $params)){
            $status=400;$msg='参数错误!';
            echo $this->call_back($status, $msg);exit;
        }
        if($params['uid']==''|| $params['money']==''){
            $status=400;$msg='参数不能为空!!';
            echo $this->call_back($status, $msg);exit;
        }
        
        $uid = trim($params['uid']);
        $userid = decryption($uid);//解密;
        //$userid =1;
        if(empty($userid)){
            $status = 400;$msg='没有该用户';
            echo $this->call_back($status, $msg);exit;
        }
        $money = $params['money'];
        if(!is_numeric($money) || $money<=0){
            $status = 400;$msg='金额有误.';
            echo $this->call_back($status, $msg);exit;
        }
        $money = round($money,2);
        if($money < 10){
            $status = 400;$msg='提现金额必须大于10元..';
            echo $this->call_back($status, $msg);exit;
        }
        
        $cols_ali = array('userid','status','infos');
        $search_where_ali[]='userid='.$userid;
        $search_where_ali[]='type="alipay"';
        $user_ali = $this->api->info_common('member_attesta',$cols_ali,$search_where_ali);
        
        if(empty($user_ali)){
            $status=500;$msg='还未绑定支付宝';
            echo $this->call_back($status, $msg);exit;
        }
        
        $alipayinfos = string2array($user_ali['infos']);
        //echo $alipayinfos['alipay_account'];exit;
        
        $cols = array('userid','modelid','money','phone','nickname');
        $search_where[]='userid='.$userid;
        $user = $this->api->info_common('member',$cols,$search_where);
        
        if(empty($user)){
            $status = 500;$msg='没有该用户!';
            echo $this->call_back($status, $msg);exit;
        }
        
        if($money > $user['money']){
            $status = 500;$msg='你的余额不足,最大提现金额为:'.$user['money'].'!!';
            echo $this->call_back($status, $msg);exit;
        }
        
        $sign = '4-1-'.$userid.'-'.$money.'-'.date('Y-m-d H:i',time());
        
        $cols_log_s = array('userid','only');
        $search_where_log_s[]='only="'.$sign.'"';
        $user_log_s = $this->api->info_common('member_finance_log',$cols_log_s,$search_where_log_s);
        if(!empty($user_log_s)){
            $status = 500;$msg='请等待一会再提现!.';
            echo $this->call_back($status, $msg);exit;
        }
        
        $cash_records= array();
    	$cash_records['userid'] = $userid;
        $cash_records['money'] = $money;
        $cash_records['inputtime'] = time();
        $cash_records['status'] = 0;
        $cash_records['ip'] = get_client_ip();
        $cash_records['cash_alipay_username'] = $alipayinfos['alipay_account'];
        $cash_records['type'] = 2;
        $cash_records['paypal'] = 1;
        $cash_records['fee'] = 0;
        $cash_records['totalmoney'] = $money;
        $cash_records['name'] = $user['nickname'].'/'.$user['phone'];
        $cash_records_add = $this->api->add_common('cash_records',$cash_records);
        if($cash_records_add['status']==200){
            $log = array();
            $log['userid'] = $userid;
            $log['num'] = -$money;
            $log['type'] = 'money';
            $log['cause'] = '用户:'.$userid.':申请提现';
            $log['dateline'] = time();
            $log['ip'] = get_client_ip();
            $log['only'] = $sign;
            $log_status = $this->api->add_common('member_finance_log',$log);
            
            if($log_status['status']==200){
                $update_info_member= array(
                    'money'=>$user['money']-$money
                );
                $update_member = $this->api->update_common('member','userid',$userid,$update_info_member);
                if($update_member['status']!=200){
                    
                }
            }else{
                
            }
        }
        echo $this->call_back($cash_records_add['status'], $cash_records_add['msg']);exit;
        
  }

  //资金明细 
  //127.0.0.1/api/api/financeLog/uid/xxxxxx/page/xxxxxx
    function financeLogAction(){
        $params = $this->getRequest()->getParams();
        if(empty($params)  || !array_key_exists('uid', $params)  || !array_key_exists('page', $params)){
            $status=400;$msg='参数错误!';
            echo $this->call_back($status, $msg);exit;
        }
        if($params['uid']==''|| $params['page']==''){
            $status=400;$msg='参数不能为空!!';
            echo $this->call_back($status, $msg);exit;
        }
        
        $uid = trim($params['uid']);
        $userid = decryption($uid);//解密;
        if(empty($userid)){
            $status = 400;$msg='没有该用户';
            echo $this->call_back($status, $msg);exit;
        }
        
        $page = intval($params['page']);
        
        if($page<=0){
            $status = 400;$msg='!分页必须大于0';
            echo $this->call_back($status, $msg);exit;
        }
        $offset = ($page - 1) * $this->pagesize;
        
        $table='member_finance_log';
        $cols = '*';
        $where_one = 'userid='.$userid;
        $where_two ='type = "money"';
        $search_where = array($where_one,$where_two);
        
        $limit = array($offset,$this->pagesize);
        $orderby = array('dateline'=>'desc');

        $res = $this->api->list_common($table,$cols,$search_where,$limit,$orderby);

        $status=200;$msg='ok';
        echo $this->call_back($status, $msg,$res);exit;
    }
  //提现记录
//127.0.0.1/api/api/cashRecords/uid/xxxxxx/page/xxxxxx
    function cashRecordsAction(){
        $params = $this->getRequest()->getParams();
        if(empty($params)  || !array_key_exists('uid', $params)  || !array_key_exists('page', $params)){
            $status=400;$msg='参数错误!';
            echo $this->call_back($status, $msg);exit;
        }
        if($params['uid']==''|| $params['page']==''){
            $status=400;$msg='参数不能为空!!';
            echo $this->call_back($status, $msg);exit;
        }
        
        $uid = trim($params['uid']);
        $userid = decryption($uid);//解密;

        if(empty($userid)){
            $status = 400;$msg='没有该用户';
            echo $this->call_back($status, $msg);exit;
        }
        
        $page = intval($params['page']);
        
        if($page<=0){
            $status = 400;$msg='!分页必须大于0';
            echo $this->call_back($status, $msg);exit;
        }
        $offset = ($page - 1) * $this->pagesize;
        
        $table='cash_records';
        $cols = '*';
        $where_one = 'userid='.$userid;
        $search_where = array($where_one);

        $limit = array($offset,$this->pagesize);
        $orderby = array('check_time'=>'desc','inputtime'=>'desc');

        $res = $this->api->list_common($table,$cols,$search_where,$limit,$orderby);

        $status=200;$msg='ok';
        echo $this->call_back($status, $msg,$res);exit;
    }
    
  //订单 status(0,2,3,4,6,7)全部 -99
    //127.0.0.1/api/api/orderList/uid/xxxxxx/order_status/xxx/page/xxxxxx
  function orderListAction(){
        $params = $this->getRequest()->getParams();
        if(empty($params)  || !array_key_exists('uid', $params) || !array_key_exists('order_status', $params) || !array_key_exists('page', $params)){
            $status=400;$msg='参数错误!';
            echo $this->call_back($status, $msg);exit;
        }
        if($params['uid']==''|| $params['order_status']==''|| $params['page']==''){
            $status=400;$msg='参数不能为空!!';
            echo $this->call_back($status, $msg);exit;
        }

        $uid = trim($params['uid']);
        $userid = decryption($uid);//解密;

        if(empty($userid)){
            $status = 400;$msg='没有该用户';
            echo $this->call_back($status, $msg);exit;
        }
        
        $order_status = $params['order_status'];
        
        if(!is_numeric($order_status)){
            $status = 400;$msg='!!!订单状态有误';
            echo $this->call_back($status, $msg);exit;
        }
        if($order_status != -99 && $order_status != 0 && $order_status != 2 && $order_status != 3 && $order_status != 4 && $order_status != 6 && $order_status != 7){
            $status = 400;$msg='!!订单状态有误';
            echo $this->call_back($status, $msg);exit;
        }
        
        $page = intval($params['page']);
        
        if($page<=0){
            $status = 400;$msg='!分页必须大于0';
            echo $this->call_back($status, $msg);exit;
        }
        $offset = ($page - 1) * $this->pagesize;
        
        $cols = array('id','order_sn','buyer_id','seller_id','goods_id','inputtime','status','check_time','act_mod','trade_sn','create_time','complete_time');
        $search_where[]='buyer_id = '.$userid;
        
        if($order_status >= 0){
            $search_where[]='status = '.$order_status;
        }
        
        $limit = array($offset,$this->pagesize);
        $orderby = array('id'=>'desc');
        $res = $this->api->list_common('order',$cols,$search_where,$limit,$orderby);
        if($res){
            foreach($res as $k=>$v){
                $product = $this->api->info_common('product',array('title','thumb','goods_price'),array('id='.$v['goods_id']));
                $store = $this->api->info_common('member_merchant',array('store_name'),array('userid='.$v['seller_id']));
                $res[$k]['product_title']=$product['title'];
                $res[$k]['product_img']=$product['thumb'];
                $res[$k]['product_price']=$product['goods_price'];
                $res[$k]['store_name']=$store['store_name'];
            }
        }
        //print_r($res);
        $status = 200;$msg='获取成功';
        echo $this->call_back($status, $msg,$res);exit;
  }
  
  
  //提交淘宝或其他订单号
  //127.0.0.1/api/api/editOrderSn/uid/xxxxxx/id/xxx/order_sn/xxxxxx
  function editOrderSnAction(){
      
        $params = $this->getRequest()->getParams();
        if(empty($params)  || !array_key_exists('uid', $params) || !array_key_exists('id', $params) || !array_key_exists('order_sn', $params)){
            $status=400;$msg='参数错误!';
            echo $this->call_back($status, $msg);exit;
        }
        if($params['uid']==''|| $params['id']==''|| $params['order_sn']==''){
            $status=400;$msg='参数不能为空!!';
            echo $this->call_back($status, $msg);exit;
        }
        
        $uid = trim($params['uid']);
        $userid = decryption($uid);//解密;

        if(empty($userid)){
            $status = 400;$msg='没有该用户';
            echo $this->call_back($status, $msg);exit;
        }
        
        $id = trim($params['id']);
        $order_sn = trim($params['order_sn']);
        
        $cols = array('status','order_sn','id','buyer_id','seller_id');
        $search_where[]='buyer_id='.$userid;
        $search_where[]='id='.$id;
        $order_info = $this->api->info_common('order',$cols,$search_where);
        if($order_info){
            if($order_info['status']!=2){
                $status = 500;$msg='该订单状态不是待填写订单号';
                echo $this->call_back($status, $msg);exit;
            }
            $order_up['order_sn'] = $order_sn;
            $order_up['status'] = 3;
            $order_up['check_time'] = time();
            $order_up['complete_time'] = time();
            $order_up_res = $this->api->update_common('order','id',$id,$order_up);
            
            if($order_up_res['status']==200){
                $order_log_add= array(
                    'order_id'=>$id,
                    'cause'=>'用户填写订单号',
                    'inputtime'=>time(),
                    'ip'=>  get_client_ip(),
                    'buyer_id'=>$userid,
                    'seller_id'=>$order_info['seller_id'],
                    'userid'=>$userid,
                    'issystem'=>0,
                );
                $order_log_add_res = $this->api->add_common('order_log',$order_log_add);
                if($order_log_add_res['status']!=200){
                    
                }
            }else{
                
            }
            echo $this->call_back($order_up_res['status'], $order_up_res['msg']);exit;
        }else{
            $status = 500;$msg='没有该订单!';
            echo $this->call_back($status, $msg);exit;
        }
  }
   
  
    //banner
  //127.0.0.1/api/api/banner
    function bannerAction(){
        
        $page = 1;
        $offset = ($page - 1) * $this->pagesize;
        $table='focus';
        $cols = '*';
        $where_one = 'starttime < '.'"'.time().'"';
        $where_two = ' (endtime > '.'"'.time().'" or endtime = 0)';
        $where_three ='basic = 2';
        $search_where = array($where_one,$where_two,$where_three);

        $limit = array($offset,$this->pagesize);
        $orderby = array('listorder'=>'asc');

        $res = $this->api->list_common($table,$cols,$search_where,$limit,$orderby);
        //print_r($res);
        $status=200;$msg='ok';
        echo $this->call_back($status, $msg,$res);exit;

    }    

    //分类
    //127.0.0.1/api/api/cateall
    function cateallAction(){

        $page = 1;
        $offset = ($page - 1) * $this->pagesize;
        $table='product_category';
        $cols = array('catid','parentid','catname','image');
        $where_one ='parentid =0';
        $where_two ='ismenu = 1';
        $search_where = array($where_one,$where_two);

        $limit = array($offset,$this->pagesize);
        $orderby = array('listorder'=>'asc');

        $res = $this->api->list_common($table,$cols,$search_where,$limit,$orderby);
        //print_r($res);
        $status=200;$msg='ok';
        echo $this->call_back($status, $msg,$res);exit;

    }

    //商品搜索关键字 page 默认 1 第一页
    //127.0.0.1/api/api/search?keyword=xxxx&page=1&type=&min_money=&max_money=
    //点击价格
    //升序
    //127.0.0.1/api/api/search?keyword=xxxx&price=asc&page=1&type=&min_money=&max_money=
    //降序
    //127.0.0.1/api/api/search?keyword=xxxx&price=desc&page=1&type=&min_money=&max_money=
    //人气
    //127.0.0.1/api/api/search?keyword=xxxx&hits=desc&page=1&type=&min_money=&max_money=
    //剩余
    //127.0.0.1/api/api/search?keyword=XXX&last=desc&page=1&type=&min_money=&max_money=
    
    //分类下的商品列表(默认排列)
    //127.0.0.1/api/api/search?cateid=0&page=1&type=&min_money=&max_money=
    //点击价格
    //升序
    //127.0.0.1/api/api/search?cateid=0&price=asc&page=1&type=&min_money=&max_money=
    //降序
    //127.0.0.1/api/api/search?cateid=0&price=desc&page=1&type=&min_money=&max_money=
    //人气
    //127.0.0.1/api/api/search?cateid=0&hits=desc&page=1&type=&min_money=&max_money=
    //剩余
    //127.0.0.1/api/api/search?cateid=0&last=desc&page=1&type=&min_money=&max_money=
    
    function searchAction(){

        if(isset($_GET['page']) && $_GET['page']!=''){
            $page = $_GET['page'];
            $page = ($page-1)>0?($page-1):1;
        }else{
            $page = 1;
        }
        $offset = ($page - 1) * $this->pagesize;
        
        $table='product as p';
        $cols = array('p.id','p.catid','p.start_time','p.end_time','p.status','p.title','p.thumb','p.already_num','t.goods_number','t.goods_price','t.goods_url','t.goods_deposit');
        $join = array('product_trial as t','p.id=t.id','left');

        if(isset($_GET['keyword']) && $_GET['keyword']!=''){
            $keyword = $_GET['keyword'];
             $where_one =" p.title like'%".$keyword."%' ";
        }else if(isset($_GET['cateid']) && $_GET['cateid']!=''){
            $where_one ='p.catid = '.intval($_GET['cateid']);
        }else{
            $where_one ='';
        }

        if(isset($_GET['type']) && $_GET['type']!=''){
            $where_two =" and t.source =".  intval($_GET['type']);
        }else{
            $where_two ='';
        }
        
        if(isset($_GET['min_money']) && $_GET['min_money']!=''){
            $where_three =" and t.goods_price >=". $_GET['min_money'];
        }else{
            $where_three='';
        }
        
        if(isset($_GET['max_money']) && $_GET['max_money']!=''){
            $where_four =" and t.goods_price <=". $_GET['max_money'];
        }else{
            $where_four ='';
        }


        $where_five = "p.start_time < "."'".time()."'";
        $where_six = "p.end_time > "."'".time()."'";
        $where_seven = "p.status = 1";
        $where_eight = "p.`mod` = 'trial'";

        $search_where = array($where_one,$where_two,$where_three,$where_four,$where_five,$where_six,$where_seven,$where_eight);
        
        $limit = array($offset,$this->pagesize);


        if(isset($_GET['price'])){
            if($_GET['price']=='asc'){//升序
                $orderby = array('t.goods_price'=>'asc');
            }else{
                $orderby = array('t.goods_price'=>'desc');
            }
        }else if(isset ($_GET['hits'])){//人气
                $orderby = array('p.hits'=>'desc');
        }else if(isset ($_GET['last'])){
            $orderby = array('t.goods_number - p.already_num'=>'desc');
        }else{//默认时间
                $orderby = array('p.start_time'=>'desc');
        }

        $res = $this->api->list_common($table,$cols,$search_where,$limit,$orderby,$join);

        //print_r($res);exit;

        $status=200;$msg='ok';
        echo $this->call_back($status, $msg,$res);exit;

    }

    //白吃,九块九包邮,品牌特卖
    //127.0.0.1/api/api/product_list/type/free/page/1
    function product_listAction(){
        $params = $this->getRequest()->getParams();
        //print_r($params);exit;
        if(empty($params) || !array_key_exists('type', $params) || !array_key_exists('page', $params)){
            $status=400;$msg='参数有误';
            echo $this->call_back($status, $msg);exit;
        }
        
        if($params['type']==''||$params['page']==''){
            $status=400;$msg='参数不能为空!';
            echo $this->call_back($status, $msg);exit;
        }
        
        if($params['type']=='free'){//白吃
            
            $value_zero = '"trial"';
            $join = array("product_trial as t", "p.id=t.id","left");
            $cols =array("p.id","p.catid","p.mod","p.start_time","p.end_time","p.title","p.thumb","p.already_num","t.goods_number","t.goods_price","t.goods_url","t.goods_deposit");
            
        }elseif($params['type']=='postal'){//包邮
            
            $value_zero = '"postal"';
            $join = array("product_postal as t", "p.id=t.id","left");
            $cols =array("p.id","p.catid","p.mod","p.start_time","p.end_time","p.title","p.thumb","t.goods_number","t.goods_price","t.price_cost","t.goods_url");
            
        }elseif($params['type']=='sale'){//特卖
            
           $value_zero = '"postaltel"';
           $join = array("product_postaltel as t", "p.id=t.id","left");
           $cols =array("p.id","p.catid","p.mod","p.start_time","p.end_time","p.title","p.thumb","t.goods_number","t.goods_price","t.price_cost","t.goods_url");
            
        }else{
            $status=500;$msg='参数有误!';
            echo $this->call_back($status, $msg);exit;
        }
        
            $table ='product as p';
           
            $field_zero ='p.mod';
  
            $where_zero = $field_zero.'='.$value_zero;
            
            $field_one ='p.status';
            $value_one = 1;
            $where_one = $field_one.'='.$value_one;
            
            $field_two ='p.start_time';
            $value_two = '"'.time().'"';
            $where_two = $field_two.'<'.$value_two;
            
            $field_three ='p.end_time';
            $value_three = '"'.time().'"';
            $where_three = $field_three.'>'.$value_three;
            
            $search_where=array($where_zero,$where_one,$where_two,$where_three);
            
            $page = intval($params['page']);

            if($page<=0){
                $status = 400;$msg='!分页必须不小于0';
                echo $this->call_back($status, $msg);exit;
            }
            $offset = ($page - 1) * $this->pagesize;

            $limit = array($offset,$this->pagesize);
            $orderby = array('p.start_time'=>'desc');
   
            $product_info = $this->api->list_common($table,$cols,$search_where,$limit,$orderby,$join);
        
            $status=200;$msg='获取成功!';
            echo $this->call_back($status, $msg,$product_info);exit;
        
    }
    
    //商品详情
  //127.0.0.1/api/api/product_detail/id/506/uid/xxxxxx(uid 可以为空)
    function product_detailAction(){
        
        $params = $this->getRequest()->getParams();
        if(empty($params) || !array_key_exists('id', $params)){
            $status=400;$msg='参数有误';
            echo $this->call_back($status, $msg);exit;
        }
        
        if($params['id']==''){
            $status=400;$msg='参数不能为空!';
            echo $this->call_back($status, $msg);exit;
        }
       
        $table ='product as p';
        $cols =array("p.id","p.status","p.userid","p.catid","p.start_time","p.end_time","p.title","p.thumb","p.already_num","p.goods_sales","t.source","t.goods_number","t.goods_price","t.price_cost","t.goods_url","t.goods_content","t.goods_deposit");
        
        $field ='p.id';
        $pro_id = (int) $params['id'];
        $where_one = $field.'='.$pro_id;
        $search_where=array($where_one);
        $join = array("product_trial as t", "p.id=t.id","left");
        $pro_info = $this->api->info_common($table,$cols,$search_where,$join);
   
        if(empty($pro_info)){
            $status=500;$msg='没有该商品';
            echo $this->call_back($status, $msg);exit;
        }
        if($pro_info['status'] !=1){
            $status=500;$msg='该商品可能已经下架!';
            echo $this->call_back($status, $msg);exit;
        }
        
        //得到商家信息(logo,name...)
        $merchant_table = 'member_merchant';
        $merchant_cols = '*';
        $merchant_field = 'userid';
        $merchant_id = $pro_info['userid'];
        
        $merchant_where_one = $merchant_field.'='.$merchant_id;
        $merchant_search_where=array($merchant_where_one);
        
        $merchant_info = $this->api->info_common($merchant_table,$merchant_cols,$merchant_search_where);
        
        //申请人数 2,7 剩余分数
        $order_status = $this->api->order_status('order',$pro_info['id']);
        //print_r($order_status);exit;
        $want_buyer =0;//已经申请的人
        $over_buyer =0;//已经完成的人
        $want_buyer_info = array();
        
        if(!empty($order_status)){
            foreach($order_status  as $k=>$v){
                if($v['status']==2){
                    $want_buyer+=1;
                }else if($v['status']==7){
                    $over_buyer+=1;
                }
                
                $url_img = APPLICATION_PATH.'/public'.getavatar($v['buyer_id']);
                if(file_exists($url_img)){
                   $want_buyer_info[$k] = 'http://www.imbaichi.com'.getavatar($v['buyer_id']);
                }else{
                    //有没有微信头像
                    $openid_where=array('userid='.$v['buyer_id']);
                    $user_weixin = $this->api->info_common('member_openid','img',$openid_where);
                    $want_buyer_info[$k] = $user_weixin['img'];
                }
            }
        }
        
        $info['img'] = $pro_info['thumb'];//图片
        $info['title'] = $pro_info['title'];//标题
        $info['price_cost'] = $pro_info['price_cost']==0?$pro_info['goods_price']:$pro_info['price_cost'];//市场价
        $info['goods_price'] = $pro_info['goods_price'];//购买价
        $info['goods_number'] = $pro_info['goods_number'];//商品分数
        $info['goods_deposit'] = $pro_info['goods_deposit'];//担保金
        $info['goods_content'] = strip_tags($pro_info['goods_content']);//描述
        $info['goods_sales'] = $pro_info['goods_sales'];//月销量
        $info['already_num'] = $pro_info['already_num'];//抢购数量
        $info['goods_number'] = $pro_info['goods_number'];//总数量
        
        $info['store_logo'] = $merchant_info['store_logo'];//商铺logo
        $info['store_name'] = $merchant_info['store_name'];//商铺名称
        
        $info['start_time'] = $pro_info['start_time'];//开始时间发起时间
        $info['end_time'] = $pro_info['end_time'];//结束时间
        
        $info['want_buyer'] = $want_buyer_info;//申请人数头像
        
        $info['want_number'] =  $want_buyer;//申请人数 1
        $info['over_number'] =  $over_buyer;//申请完成分数  7
        $info['last_number'] = $pro_info['goods_number']-$over_buyer-$want_buyer;//剩余分数
        
        $info['back_money'] = $pro_info['goods_price'];//退回购物金额
        $info['goods_url'] = $pro_info['goods_url'];//下单地址
        $info['source'] = $pro_info['source'];//下单类型
        
        
        //判断活动时间如果结束修改状态为 2 结算中
        if($pro_info['end_time']< time()){
            if($pro_info['status']==1){
                $update_info = $this->api->update_common('product','id',$pro_info['id'],array('status'=>2));
                $pro_log = array(
                    'p_id'=>$pro_info['id'],
                     'p_state'=>2,
                     'is_sys'=>0,
                     'uid'=>$pro_info['id'],
                     'msg'=>'产品下架时间已到',
                     'dateline'=>time(),
                     'clientip'=>  get_client_ip(),
                );
                $pro_log = $this->api->add_common('product_log',$pro_log);
            }
        }
        
        if(array_key_exists('uid', $params) && $params['uid']!=''){
            
            $userid = $params['uid'];
            if(strlen($params['uid'])>18){
                 $userid = decryption($userid);//解密;
            }
            
            //是否购买和状态
            $order_table = 'order';
            $order_cols = 'status';
            $order_field_one = 'buyer_id';
            $buyer_id = $userid;
            $order_field_two = 'goods_id';
            $goods_id = $params['id'];
            $where_one = $order_field_one.'='.$buyer_id;
            $where_two = $order_field_two.'='.$goods_id;
            $order_search_where=array($where_one,$where_two);
            $order_info = $this->api->info_common($order_table,$order_cols,$order_search_where);
            
            if(empty($order_info)){
                $info['type'] = 0;//可以购买
                $info['status'] = 0;//可以购买
            }else{
                $info['type'] = 1;//不可以购买
                $info['status'] = $order_info['status'];//订单状态：0：已关闭(没有抢到默认0)，2：已确认（订单/资格,如果抢到默认2），3：待审核，4：审核失败，6：申诉中，7：已完成
            }
        }else{
                $info['type'] = 1;//不可以购买
                $info['status'] = -99;//不可以购买没有登录
        }
       
        $status=200;$msg='获取成功';
        echo $this->call_back($status, $msg,$info);exit;
        
        return false;
    }
    
//购买
  //127.0.0.1/api/api/paySubmitApp/uid/xxxx/goods_id/xxxx/phone/xxxxx
    //uid 没有 请设置 -99  phone 没有也请设置为 -99
    function paySubmitAppAction(){
        
        $params = $this->getRequest()->getParams();
        //print_r($params);
        if(empty($params)  || !array_key_exists('uid', $params)  || !array_key_exists('goods_id', $params)|| !array_key_exists('phone', $params)){
            $status=400;$msg='参数错误!';
            echo $this->call_back($status, $msg);exit;
        }
        if($params['uid']==''|| $params['goods_id']==''|| $params['phone']==''){
            $status=400;$msg='参数不能为空!!';
            echo $this->call_back($status, $msg);exit;
        }
        
        $goods_id = trim($params['goods_id']);
        if(!is_numeric($goods_id) || $goods_id <= 0){
            $status=400;$msg='商品有误.';
            echo $this->call_back($status, $msg);exit;
        }
        
        $table ='product as p';
        $cols =array("p.id","p.start_time","p.end_time","p.status","p.userid","p.catid","p.already_num","t.goods_number","t.goods_price","t.probability");
        
        $field ='p.id';
        $pro_id = (int) $params['goods_id'];
        $where_one = $field.'='.$pro_id;
        $search_where=array($where_one);
        $join = array("product_trial as t", "p.id=t.id","left");
        $pro_info = $this->api->info_common($table,$cols,$search_where,$join);
   
        if(empty($pro_info)){
            $status=500;$msg='没有该商品.!';
            echo $this->call_back($status, $msg);exit;
        }
        if($pro_info['status'] !=1){
            $status=500;$msg='该商品已经下架!.';
            echo $this->call_back($status, $msg);exit;
        }
        if($pro_info['start_time'] > time()){
            $status=500;$msg='该商品还未上架!..';
            echo $this->call_back($status, $msg);exit;
        }
        
        if($pro_info['end_time'] < time()){
            $status=500;$msg='该商品已经下架!...';
            echo $this->call_back($status, $msg);exit;
        }
        
        if($pro_info['already_num'] >= $pro_info['goods_number']){
            $status=500;$msg='该商品库存不足!!';
            echo $this->call_back($status, $msg);exit;
        }
        
        $uid = trim($params['uid']);
        $phone = trim($params['phone']);
        
        if($uid != -99  && $phone == -99){
            $userid = decryption($uid);//解密;
            
            if(empty($userid)){
                $status = 400;$msg='.没有该用户';
                echo $this->call_back($status, $msg);exit;
            }
        }elseif($uid != -99 && $phone != -99){
            $userid = decryption($uid);//解密;
          
            if(empty($userid)){
                $status = 400;$msg='..没有该用户';
                echo $this->call_back($status, $msg);exit;
            }
        }elseif($uid == -99 && $phone != -99){
            if(!is_numeric($phone) || strlen($phone) < 11 || strlen($phone) > 11){
                $status=400;$msg='手机号码有误.!';
                echo $this->call_back($status, $msg);exit;
            }
            $member_where[] = 'phone="'.$phone.'"';
            $user_info = $this->api->info_common('member',array('userid','phone'),$member_where);
            if($user_info){
                $userid = $user_info['userid'];
            }else{
                    $encrypt = random(6);
                    $password = md5(md5($encrypt.$encrypt));
                    $client_ip = get_client_ip();

                    $info['modelid'] = 1;
                    $info['encrypt'] = $encrypt;
                    $info['password'] = $password;
                    $info['nickname'] = $phone;
                    $info['phone'] =    $phone;
                    $info['regdate'] = time();
                    $info['regip'] = $client_ip;
                    $info['lastdate'] = time();
                    $info['lastip'] = $client_ip;
                    $info['loginnum'] =1;
                    $info['islock'] = 0;
                    $info['point'] = (int) 0;
                    $info['groupid'] = 1;
                    $info['phone_status']=1;
                   $add = $this->api->add_common('member',$info);
                   if($add['status']!=200){
                        echo $this->call_back($add['status'],$add['msg']);exit;
                   }
                    $userid = $add['id'];
                   //$userid = encryption($add['id']);//加密过的userid
            }
        }elseif($uid == -99 && $phone == -99){
            $status = 400;$msg='....用户信息有误';
            echo $this->call_back($status, $msg);exit;
        }
        
        if($pro_info['userid'] == $userid){
            $status = 500;$msg='商家不能购买自家商品';
            echo $this->call_back($status, $msg);exit;
        }
        
        $member_where_s[] = 'userid='.$userid;
        $user_info_s = $this->api->info_common('member',array('userid','modelid'),$member_where_s);
        
        if(empty($user_info_s)){
            $status = 400;$msg='....用户信息有误!';
            echo $this->call_back($status, $msg);exit;
        }
        
            $member_where_order[] = 'buyer_id='.$userid;
            $member_where_order[] = 'goods_id='.$goods_id;
            $order_info = $this->api->info_common('order',array('status'),$member_where_order);
            
            if(!empty($order_info)){
                $order_status = $order_info['status'];
                if($order_status == 2){//抽中没有填订单号
                    $msg='恭喜您，已获得白吃资格，请抓紧时间参照白吃玩法领取属于您的免费白吃！';
                    $msg_button='恭喜您，已获得';
                }elseif($order_status == 3){//填写订单号待审核
                    $msg='您的订单号商家正在审核，请耐心等待……';
                    $msg_button='订单号审核中';
                }elseif($order_status == 4){//填写订单号审核没通过
                    $msg='很遗憾，您的订单号未通过商家审核，请珍惜您的白吃资格，如要申诉，请联系天天白吃在线客服QQ：825698173！';
                    $msg_button='审核不通过';
                }elseif($order_status == 6){//填写订单号 商家不通过,用户申诉
                    $msg_button='正在申诉';
                    $msg='正在申诉中，请耐心等待，或联系天天白吃官方在线客服：825698173';
                }elseif($order_status == 7){//彻底完成
                    $msg='本轮白吃已经完成！欢迎您参加更多的白吃。';
                    $msg_button='已成功白吃';
                }else{//0 没抽中或被关闭
                    $msg='很遗憾，您本轮未获得白吃资格，一定要再接再厉喔！';
                    $msg_button='很遗憾，未获得';
                }
                $status = 201;
                $res_order = array();
                $res_order['msg_button']=$msg_button;
                echo $this->call_back($status, $msg,$res_order);exit;
            }
        
            
        $yac = new Yac();
        $yac_receive_name = 'order_receive_'.$goods_id;  
        //$yac->set($yac_receive_name,0);exit; 
      
        $probability = $pro_info['probability'];//概率
        $goods_number = $pro_info['goods_number'];//份数
         
        if($probability<=0){
            $new_order_status = 0;
            $new_order_msg ='没有获取白吃资格';
        }else{
            $yac_receive_count = $yac->get($yac_receive_name);
            if($yac_receive_count){
                $order_count = $yac_receive_count;
            }else{
                $order_count = $this->api->order_count('order','goods_id',$goods_id,array(2,7));
            }
            if($order_count >= $goods_number){
                $status = 500;$msg='商品库存不足';
                echo $this->call_back($status, $msg);exit;
            }else{
                if($probability ==1){
                    $new_order_status = 2;
                    $new_order_msg ='购买完成后记得反馈订单号回来返现噢';
                }else{
                    
                    $pro_lv = $goods_number/$probability;
                    $pro_lv = round($pro_lv);
                    $rand = mt_rand(1, $pro_lv);
                    //echo $pro_lv;
                    //echo $rand;
                    if ($rand == 1) {//假如第一个为中奖
                        if($order_count >= $goods_number){
                            $new_order_status = 0;
                            $new_order_msg ='没有获取白吃资格';
                        }else{
                            $new_order_status = 2;
                            $new_order_msg ='购买完成后记得反馈订单号回来返现噢';
                        }
                    } else {
                        $new_order_status = 0;
                        $new_order_msg ='没有获取白吃资格';
                    }
                }
            }
        }
        
                $new_order_add = array(
                    'buyer_id'=>$userid,
                    'seller_id'=>$pro_info['userid'],
                    'goods_id'=>$goods_id,
                    'act_mod'=>'trial',
                    'trade_sn'=>date('YmdHis').random(6,1),
                    'inputtime'=>time(),
                    'create_time'=>time(),
                    'check_time'=>time(),
                    'complete_time'=>time(),
                    'status'=>$new_order_status,
                    'new_status'=>$new_order_status,
                );
                
                $new_order_add_res = $this->api->add_common('order',$new_order_add);
                if($new_order_add_res['status']==200){
                    $order_new_id = $new_order_add_res['id'];
                    
                    $new_order_log['order_id']=$order_new_id;
                    $new_order_log['buyer_id']=$userid;
                    $new_order_log['seller_id']=$pro_info['userid'];
                    $new_order_log['cause'] = $new_order_msg;
                    $new_order_log['inputtime'] = time();
                    $new_order_log['ip']= get_client_ip();
                    $new_order_log['userid'] = $userid;
                    
                    $new_order_log_res = $this->api->add_common('order_log',$new_order_log);
                    
                    if( $new_order_log_res['status']!=200){
                        
                    }
                    $yac_order_count = $order_count+1;
                    $yac->set($yac_receive_name,$yac_order_count);
                    
                    $status = 200;$msg=$new_order_msg;
                    echo $this->call_back($status, $msg);exit;
                }else{
                    $status = 200;$msg='没有获取白吃资格';
                    echo $this->call_back($status, $msg);exit;
                }
                
    }
    
    
    //白吃说
    //127.0.0.1/api/api/gongLue/read/0/page/1
        function gongLueAction(){
        $params = $this->getRequest()->getParams();
        if(empty($params) || !array_key_exists('page', $params)|| !array_key_exists('read', $params)){
            $status=400;$msg='参数错误!';
            echo $this->call_back($status, $msg);exit;
        }
        if($params['page']==''|| $params['read']==''){
            $status=400;$msg='参数不能为空!!';
            echo $this->call_back($status, $msg);exit;
        }
        $page = intval($params['page']);
        
        if($page<=0){
            $status = 400;$msg='!分页必须不小于0';
            echo $this->call_back($status, $msg);exit;
        }
        $offset = ($page - 1) * $this->pagesize;
        
                
        $table='gonglue';
        $cols = array('id','title','thumb','description','`like`','`read`');
        $where_one ='shows_index = 0';
        $search_where = array($where_one);

        $limit = array($offset,$this->pagesize);
        if($params['read']==1){
            $orderby = array('`read`'=>'desc','`like`'=>'desc','id'=>'desc');
        }else{
            $orderby = array('listorder'=>'asc','id'=>'desc');
        }

        $res = $this->api->list_common($table,$cols,$search_where,$limit,$orderby);
        //print_r($res);
        $status=200;$msg='ok';
        echo $this->call_back($status, $msg,$res);exit;
        
    }
    
    
    
}
