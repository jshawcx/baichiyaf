<?php
/**
 * @name SamplePlugin
 * @desc Yaf定义了如下的6个Hook,插件之间的执行顺序是先进先Call
 * @see http://www.php.net/manual/en/class.yaf-plugin-abstract.php
 * @author root
 */
class SamplePlugin extends Yaf_Plugin_Abstract {

	public function routerStartup(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
            
	}

	public function routerShutdown(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
                //去掉 Module 后的 index,比如  goyaf.com/admin/index/index 可以写成goyaf.com/admin/index
                $modules = Yaf_Application::app()->getModules();
                $uri = $request->getServer();
                $uriInfo = explode('/', $uri['REQUEST_URI']);
                $module = ucfirst($uriInfo[1]);
                //echo $module;exit;
                //print_r($uriInfo);
//                echo $request->controller;
//                echo '<br/>';
//                echo  $request->action;
//                echo '<br/>';
                if(!in_array($module, $modules)){//module名不在规定的默认module中
                    //print_r($modules);
                     //$uriInfo[1]//controller 名
                    //$uriInfo[2]//action  名
                    
                    if($module == 'Login'){
                        $module = 'index';
                        //$request->setModuleName($module);
                        if(isset($uriInfo[1]) && $uriInfo[1]!=''){
                            $controller = $uriInfo[1];
                        }else{
                            $controller = 'index';
                        }
                        $request->setControllerName(ucfirst($controller));
                        if(isset($uriInfo[2]) && $uriInfo[2]!=''){
                            $action = $uriInfo[2];
                        }else{
                            $action = 'index';
                        }
                        $request->setActionName($action); 
                    }
                }else{//有写module
                    //$uriInfo[1]//module 名
                    //$uriInfo[2]//controller  名
                    //$uriInfo[3]//action  名
                    $request->setModuleName($module);
                    
                    if(isset($uriInfo[2]) && $uriInfo[2]!=''){
                        $controller = $uriInfo[2];
                    }else{
                        $controller = 'index';
                    }
                    
                    $request->setControllerName(ucfirst($controller));

                    if(isset($uriInfo[3]) && $uriInfo[3]!=''){
                        $action = $uriInfo[3];
                    }else{
                        $action = 'index';
                    }

                    $request->setActionName($action);
                }
            
	}

	public function dispatchLoopStartup(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
	}

	public function preDispatch(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
	}

	public function postDispatch(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
	}

	public function dispatchLoopShutdown(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
	}
}
