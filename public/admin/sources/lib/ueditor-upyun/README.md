﻿# ueditor-for-UPYUN

UEditor是由百度web前端研发部开发所见即所得富文本web编辑器，具有轻量，可定制，注重用户体验等特点，开源基于MIT协议，允许自由使用和修改代码。

当上传图片时，允许用户上传到自己配置好的UPYUN空间里。

只需配置 `php/imageUp.php` 的以下三个参数即可：

```
$bucket='';	//空间bucket名
$upyun_user=''; //用户名
$upyun_psw='';	//密码
```

如果不配置，则使用原生方法上传到自己本地空间。

[![UPYUN](http://upfiles.b0.upaiyun.com/logo/180x90.png)](https://www.upyun.com/)