<?php
    header("Content-Type:text/html;charset=utf-8");
    error_reporting( E_ERROR | E_WARNING );
    date_default_timezone_set("Asia/chongqing");
    include "Uploader.class.php";
    
    require_once (dirname($_SERVER['DOCUMENT_ROOT']).'/application/library/upyun.class.php');
    
            $dir_ini = dirname($_SERVER['DOCUMENT_ROOT']).'/conf/application.ini';
            $config = new Yaf_Config_Ini($dir_ini,'account');
       
            $bucket = $config->upyun->bucket;
            $upyun_user = $config->upyun->user;
            $upyun_psw = $config->upyun->pwd;

        
	if($bucket&&$upyun_user&&$upyun_psw){
		$upyun = new UpYun($bucket, $upyun_user, $upyun_psw);
		try {
			$fh = $fh = fopen($_FILES[ "upfile" ][ 'tmp_name' ], 'rb');
			$oldname=$_FILES[ "upfile" ]["name"];
			$filesize=$_FILES['upfile']['size'] ;
			$filetype = pathinfo($oldname, PATHINFO_EXTENSION);
			$newname=getName() .'.'.$filetype;
			$newfileurl='/'. getFolder() .'/'.$newname;
			$upinfo = $upyun->writeFile($newfileurl, $fh, True);   // 上传图片，自动创建目录
			fclose($fh);
			$info=array(
                            "originalName" => $oldname ,
                            "name" => $newname ,
                            "url" => "http://".$bucket.".b0.upaiyun.com".$newfileurl ,
                            "size" => $filesize ,
                            "type" => $filetype ,
                            "state" => 'SUCCESS'
                        );
		}
		catch(Exception $e) {
			echo $e->getCode();
			echo $e->getMessage();
		}
	}else{
		//上传配置
		$config = array(
			"maxSize" => 2000 ,                   //允许的文件最大尺寸，单位KB
			"allowFiles" => array( ".gif" , ".png" , ".jpg" , ".jpeg" , ".bmp" )  //允许的文件格式
		);
		//上传文件目录
		$Path = "/upload/";
		//背景保存在临时目录中
		$config[ "savePath" ] = $Path;
		$up = new Uploader( "upfile" , $config );
		$type = $_REQUEST['type'];
		$callback=$_GET['callback'];
	
		$info = $up->getFileInfo();
	}
    /**
     * 返回数据
     */
    if($callback) {
        echo '<script>'.$callback.'('.json_encode($info).')</script>';
    } else {
        echo json_encode($info);
    }
	
	function getFolder()
    {
        $pathStr = 'upload';
        if ( strrchr( $pathStr , "/" ) != "/" ) {
            $pathStr .= "/";
        }
        $pathStr .= date( "Ymd" );
        return $pathStr;
    }
	function getName()
    {
        return time() . rand( 1 , 10000 );
    }