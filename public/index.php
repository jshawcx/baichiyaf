<?php
header('content-Type:text/html;charset=utf-8;');
//防注入
foreach (array("'", '%27', '"', '%22', '(', '%28', '*', '%2A', '.php', '.js', '.css', '.png', '.jpg', '.gif', '.txt', '.ico', '.ini', '.ssh', '.svn', '.log') as $k1 => $v1){
	if(false !== strpos($_SERVER['REQUEST_URI'], $v1)) {
		header("HTTP/1.1 204"); exit;
	}
}

//检查是否安装了yaf扩展
if(!extension_loaded("yaf")) die('Not Install Yaf');

define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../'));

$application = new Yaf_Application( APPLICATION_PATH . "/conf/application.ini",ini_get('yaf.environ'));

$application->bootstrap()->run();
?>
